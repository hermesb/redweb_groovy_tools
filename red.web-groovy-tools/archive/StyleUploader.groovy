import distantvoices3.client.BasicClient
import eu.red_web.layout.pictogram.controller.PictogramController
import eu.red_web.library.compression.CompressionUtil
import eu.red_web.server.client.ClientFactory


def server = "192.168.119.129"
def user = "hermesb"
def passwd = "hermesb"
def userHome = System.getProperty("user.home")
def enc = "UTF-8"
def cssSrcDir = /d:\temp\cssArticleLayouts/
def clientID = "4d234d618df284b776b5dab0ad3ef6701936b60f693d4cdf3076fd6b684f5b73"
def publicationID = "745dbed41901eb62a91b26a9c9e8374360fed3ab5441ff5bb9fc73fedd0cdbfc"

//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, 8100, user, null);
ClientFactory f = new ClientFactory(cl);
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null);
if (keys[1]==null)
  throw new Exception(keys[3]);

cl.setPassword(keys[1]);

println "--------------------- unlink stylesheets from pictos --------------------------------"

try {
  f.getTransactionDVProxy().open();

  def allPictoIDs = f.getPictogramDVProxy().getIDs(publicationID)
  allPictoIDs.each{ pictoID->
    def pictoBytes = f.getPictogramDVProxy().download(pictoID)
    def pictoXML = new String(CompressionUtil.inflate(pictoBytes),enc);
    PictogramController newPc = this.getClass().getClassLoader().loadClass("eu.red_web.layout.pictogram.controller.PictogramController").newInstance()
    newPc.fromDocument(pictoXML, true)
    def oldIDList = new ArrayList(newPc.getSelectedStylesheetIdList())
    oldIDList.each{oldID->
      newPc.removeStylesheetId(oldID)
    }
    newPc.setActiveStylesheet(null)
    println "Piktogram ${pictoID} stylelist length: ${newPc.getStylesheetsController().getSelectedIds().size()}"
    byte[] newPCAsBytes = newPc.toDocument()
    f.getPictogramDVProxy().upload(CompressionUtil.deflate(newPCAsBytes),clientID,publicationID)
  }
  f.getTransactionDVProxy().commit();
} catch (Exception e) {
  try {
    f.getTransactionDVProxy().rollback();
  } catch (Exception e2) {
  }
  throw e;
}

println "--------------------- upload stylesheets and link with pictos --------------------------------"

try{
  new File(cssSrcDir).eachFileMatch(~/.*\.css/){ file->
    String styleContent = file.getText(enc);
    def fileNameWithoutExt = file.name.replaceAll("\\.css", "")
    //println "${fileNameWithoutExt}:\n${styleContent}\n............................................................................................"
    def styleID = fileNameWithoutExt
    def startString = "/*start picto ids"
    def endString = "end picto ids*/"
    def pictoIDBlockStart = styleContent.indexOf(startString)
    def pictoIDBlockEnd = styleContent.indexOf(endString)
    def pictoIDs = ""
    if(pictoIDBlockStart >= 0 && pictoIDBlockEnd >=0){
      def tempStart = pictoIDBlockStart + startString.length() + 1 < styleContent.length() ? pictoIDBlockStart + startString.length() + 1 : pictoIDBlockStart + startString.length()
      def tempEnd = pictoIDBlockEnd + endString.length() + 1 < styleContent.length() ? pictoIDBlockEnd + endString.length() + 1 : pictoIDBlockEnd + endString.length()
      pictoIDs = styleContent.substring(tempStart, pictoIDBlockEnd)
      styleContent = styleContent.substring(tempEnd)
      println "style: ${styleID}\n${styleContent} associated with:\n ${pictoIDs}"
    }
    f.getTransactionDVProxy().open()
    byte[] styleContentAsBytes = styleContent.getBytes(enc)
    f.getStyleDVProxy().upload(CompressionUtil.deflate(styleContentAsBytes),styleID,clientID,publicationID,styleID);
    f.getTransactionDVProxy().commit();

    f.getTransactionDVProxy().open()
    pictoIDs.split("\n").eachWithIndex{pictoID,index->
      println "processing picto: ${pictoID}"
      byte[] pictoBytes = null
      try{
        pictoBytes = f.getPictogramDVProxy().download(pictoID)
      }catch(RemoteException){
        println "es existiert kein Pictogram mit der ID ${pictoID}"
      }
      if(pictoBytes != null && pictoBytes.size() > 0){
        def pictoXML = new String(CompressionUtil.inflate(pictoBytes),enc)
        println "picto id: ${pictoID} name: ${f.getPictogramDVProxy().getName(pictoID,0)}_${f.getPictogramDVProxy().getName(pictoID,1)}_${f.getPictogramDVProxy().getName(pictoID,2)}"
        PictogramController newPc = this.getClass().getClassLoader().loadClass("eu.red_web.layout.pictogram.controller.PictogramController").newInstance()
        newPc.fromDocument(pictoXML, true)
        newPc.setUseStylesheets(true);
        newPc.addSelectedStylesheetId(styleID)
        byte[] newPCAsBytes = newPc.toDocument()
        String pictoXMLWithStyles = new String(newPCAsBytes,enc)
        //println "styleID: ${styleID}:\n${pictoXMLWithStyles}"
        f.getPictogramDVProxy().upload(CompressionUtil.deflate(newPCAsBytes),clientID,publicationID)
      }
    }
    f.getTransactionDVProxy().commit();
    println "--------------------------------------------------------------------------------------";
  }
} catch (Exception e) {
  try {
    f.getTransactionDVProxy().rollback();
  } catch (Exception e2) {
  }
  throw e;
}

println "--------------------- set first stylesheet active --------------------------------"

try{
  f.getTransactionDVProxy().open()
  allPictoIDs = f.getPictogramDVProxy().getIDs(publicationID)
  allPictoIDs.each{ pictoID->
    def pictoBytes = f.getPictogramDVProxy().download(pictoID)
    def pictoXML = new String(CompressionUtil.inflate(pictoBytes),enc);
    PictogramController newPc = this.getClass().getClassLoader().loadClass("eu.red_web.layout.pictogram.controller.PictogramController").newInstance()
    newPc.fromDocument(pictoXML, true)
    def selStyleIds = newPc.getSelectedStylesheetIdList()
    if(selStyleIds.size > 0){
      newPc.setActiveStylesheet(selStyleIds[0])
      println "Piktogram ${pictoID} active style: ${newPc.getStylesheetsController().getActivatedStylesheetId()}"
    }
    byte[] newPCAsBytes = newPc.toDocument()
    String pictoXMLNew = new String(newPCAsBytes,enc)
    println "uploading pictogram ${pictoID}\n${pictoXMLNew}"
    f.getPictogramDVProxy().upload(CompressionUtil.deflate(newPCAsBytes),clientID,publicationID)
  }
  f.getTransactionDVProxy().commit();
} catch (Exception e) {
  try {
    f.getTransactionDVProxy().rollback();
  } catch (Exception e2) {
  }
  throw e;
}


