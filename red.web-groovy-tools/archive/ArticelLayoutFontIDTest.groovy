import groovy.sql.Sql

def sql = Sql.newInstance("jdbc:derby:C:\\Users\\hermes/.redweb/redweb-p01.rnz.de/database/redweb30;create=false", "","", "org.apache.derby.jdbc.AutoloadedDriver")
def redwebHome = /c:\Users\hermes\.redweb\redweb-p01.rnz.de\filestore/
//def sql = Sql.newInstance("jdbc:derby:C:\\Users\\hermes/.redweb/172.20.90.85/database/redweb30;create=false", "","", "org.apache.derby.jdbc.AutoloadedDriver")
//def redwebHome = /c:\Users\hermes\.redweb\172.20.90.85\filestore/

def publicationsMap = [:]
def publicationsIDSQL = "SELECT RW_PUBLICATION.ID AS ID,RW_PUBLICATION.NAME AS NAME ,RW_CLIENT.NAME AS CLIENT FROM RW_PUBLICATION,RW_CLIENT WHERE RW_PUBLICATION.CLIENTID = RW_CLIENT.ID"
sql.eachRow(publicationsIDSQL) {
  publicationsMap[it.ID] = "${it.CLIENT}\\${it.NAME}"
}

publicationsMap.each { publicationsID,publicationClientAndName ->
  //def publicationsID = key.toString();
  def piktoSrcDir = "${redwebHome}\\${publicationClientAndName}\\pictogram"
  def makroSrcDir = "${redwebHome}\\${publicationClientAndName}\\macro"

  def pictoSQL = "SELECT ID,NAME1,NAME2,NAME3,ACTIVE FROM RW_PICTOGRAM WHERE PUBLICATIONID LIKE ${publicationsID} AND MARKEDFORDELETION = 0"
  def pictoMap = [:];
  sql.eachRow(pictoSQL) {
    pictoMap[it.ID] = "${it.NAME1}_${it.NAME2}_${it.NAME3}".padRight(85," ") + "${it.ACTIVE.asBoolean() ? 'aktiv' : 'inaktiv'}".padRight(8," ")
  }

  def macroSQL = "SELECT ENTITYID,NAME,ACTIVE,MACROGROUP FROM NAME,RW_MACRO WHERE PUBLICATIONID LIKE ${publicationsID} AND NAME.ENTITYID = RW_MACRO.ID AND MARKEDFORDELETION = 0"
  def macroMap = [:];
  sql.eachRow(macroSQL) {
    macroMap[it.ENTITYID] = "${it.MACROGROUP}>${it.NAME}".padRight(90," ") +  "${it.ACTIVE.asBoolean() ? 'aktiv' : 'inaktiv'}".padRight(20," ");
  }

  def fontSQL = "SELECT ID,PSNAME FROM RW_FONT WHERE PUBLICATIONID LIKE ${publicationsID} "
  def fontIDMap = [:];
  def fontNameMap = [:]
  sql.eachRow(fontSQL) {
    fontIDMap[it.ID] = it.PSNAME
    fontNameMap[it.PSNAME] = it.ID
  }

  def colorSQL = "SELECT ID,PSNAME FROM RW_COLOR WHERE PUBLICATIONID LIKE ${publicationsID} "
  def colorIDMap = [:];
  def colorNameMap = [:]
  sql.eachRow(colorSQL) {
    colorIDMap[it.ID] = it.PSNAME
    colorNameMap[it.PSNAME] = it.ID
  }

  println "Teste ${publicationClientAndName}"
  println "Teste Piktogramme:"
  def errorList = [:];
  new File(piktoSrcDir).eachFileMatch(~/.*\.xml/){file->
    if(!file.name.startsWith("thumb")){
      def content = file.getText("UTF-8");
      def pattern = ~/ca:Font" value="([^"]*)"/
      pattern.matcher(content).each {
        def fontID = it[1]
        if(fontIDMap[fontID] == null || fontIDMap[fontID].toString().isEmpty()){
          String pictoID = file.name.toString().substring(0, file.name.toString().length()-4);
          errorList[pictoID] = "${pictoMap[pictoID]} (${pictoID}) invalid font id ${fontIDMap[fontID]}"
        }
      }
    }
  }
  if(errorList.size() > 0){
    println  "Fonts in Piktogrammen fehlerhaft:\n"
    errorList = errorList.sort {it.value}
    errorList.each {it,val->println val}
    println ""
  }else{
    println "Fonts in Piktogrammen in Ordnung"
  }

  errorList = [:];
  new File(piktoSrcDir).eachFileMatch(~/.*\.xml/){file->
    if(!file.name.startsWith("thumb")){
      def content = file.getText("UTF-8");
      def pattern = ~/ca:FontColor" value="([^"]*)"/
      pattern.matcher(content).each {
        def colorID = it[1]
        if(colorIDMap[colorID] == null || colorIDMap[colorID].toString().isEmpty()){
          String pictoID = file.name.toString().substring(0, file.name.toString().length()-4);
          errorList[pictoID] = "${pictoMap[pictoID]} (${pictoID}) invalid color id ${colorIDMap[colorID]}"
          result = false;
        }
      }
    }
  }
  if(errorList.size() > 0){
    println  "Farben in Piktogrammen fehlerhaft:\n"
    errorList = errorList.sort {it.value}
    errorList.each {it,val->println val}
    println ""
  }else{
    println "Farben in Piktogrammen in Ordnung"
  }

  errorList = [:];
  new File(piktoSrcDir).eachFileMatch(~/.*\.xml/){file->
    if(!file.name.startsWith("thumb")){
      def content = file.getText("UTF-8");
      def pattern = ~/ca:FontBKColor" value="([^"]*)"/
      pattern.matcher(content).each {
        def colorID = it[1]
        if(!colorID.toString().isEmpty() &&  (colorIDMap[colorID] == null || colorIDMap[colorID].toString().isEmpty())){
          String pictoID = file.name.toString().substring(0, file.name.toString().length()-4);
          errorList[pictoID] = "${pictoMap[pictoID]} (${pictoID}) invalid color id ${colorIDMap[colorID]}"
          result = false;
        }
      }
    }
  }
  if(errorList.size() > 0){
    println  "Hintergrundfarben in Piktogrammen fehlerhaft:\n"
    errorList = errorList.sort {it.value}
    errorList.each {it,val->println val}
    println ""
  }else{
    println "Hintergrundfarben in Piktogrammen in Ordnung"
  }


  println "Teste Makros:"
  errorList = [:];
  new File(makroSrcDir).eachFileMatch(~/.*\.js/){file->
    def content = file.getText("ISO-8859-1")
    def pattern = ~"setCaFontByName\\(\"([^\"]*)\""
    pattern.matcher(content).each {
      def fontName = it[1]
      if(fontNameMap[fontName] == null || fontNameMap[fontName].toString().isEmpty()){
        String macroID = file.name.toString().substring(0, file.name.toString().length()-3);
        errorList[macroID] = "${macroMap[macroID]} id: ${macroID}\t\t\tinvalid font id ${fontNameMap[fontName]}"
        result = false;
      }
    }
  }
  if(errorList.size() > 0){
    println  "Fonts in Makros: fehlerhaft:\n"
    errorList = errorList.sort {it.value}
    errorList.each {it,val->println val}
    println ""
  }else{
    println "Fonts in Makros: in Ordnung"
  }

  errorList = [:];
  new File(makroSrcDir).eachFileMatch(~/.*\.js/){file->
    def content = file.getText("ISO-8859-1")
    def pattern = ~"setCaFontColorByName\\(\"([^\"]*)\""
    pattern.matcher(content).each {
      def colorName = it[1]
      if(colorNameMap[colorName] == null || colorNameMap[colorName].toString().isEmpty()){
        String macroID = file.name.toString().substring(0, file.name.toString().length()-3);
        errorList[macroID] = "${macroMap[macroID]} id: ${macroID}\t\t\tinvalid color id ${colorNameMap[colorName]}"
        result = false;
      }
    }
  }
  if(errorList.size() > 0){
    println  "Zeichenfarben in Makros: fehlerhaft:\n"
    errorList = errorList.sort {it.value}
    errorList.each {it,val->println val}
    println ""
  }else{
    println "Zeichenfarben in Makros: in Ordnung"
  }

  errorList = [:];
  new File(makroSrcDir).eachFileMatch(~/.*\.js/){file->
    def content = file.getText("ISO-8859-1")
    def pattern = ~"setCaFontBKColorByName\\(\"([^\"]*)\""
    pattern.matcher(content).each {
      def colorName = it[1]
      if(colorNameMap[colorName] == null || colorNameMap[colorName].toString().isEmpty()){
        String macroID = file.name.toString().substring(0, file.name.toString().length()-3);
        errorList[macroID] = "${macroMap[macroID]} id: ${macroID}\t\t\tinvalid color id ${colorNameMap[colorName]}"
        result = false;
      }
    }
  }
  if(errorList.size() > 0){
    println  "Hintergrund-Zeichenfarben in Makros: fehlerhaft:\n"
    errorList = errorList.sort {it.value}
    errorList.each {it,val->println val}
    println ""
  }else{
    println "Hintergrund-Zeichenfarben in Makros: in Ordnung"
  }

  errorList = [:];
  new File(makroSrcDir).eachFileMatch(~/.*\.js/){file->
    def content = file.getText("ISO-8859-1")
    def pattern = ~"color=([^,]*)"
    pattern.matcher(content).each {
      def colorName = it[1]
      if(colorNameMap[colorName] == null || colorNameMap[colorName].toString().isEmpty()){
        String macroID = file.name.toString().substring(0, file.name.toString().length()-3);
        errorList[macroID] = "${macroMap[macroID]} id: ${macroID}\t\t\tinvalid color id ${colorNameMap[colorName]}"
        result = false;
      }
    }
  }
  if(errorList.size() > 0){
    println  "Linienfarben in Makros: fehlerhaft:\n"
    errorList = errorList.sort {it.value}
    errorList.each {it,val->println val}
    println ""
  }else{
    println "Linienfarben in Makros: in Ordnung"
  }
}
