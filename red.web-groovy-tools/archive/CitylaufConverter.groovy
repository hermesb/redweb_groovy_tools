import java.io.File;


class CitylaufConverter{
	
	static void main(args) {
		def srcDir = /D:\Temp\citylauf\source/;
		def targetDir = /D:\Temp\citylauf\target/;
		def file = new File(srcDir).eachFileMatch(~/.*\.rws/){file->
			convert(file,targetDir);
		}
	}
	
	static void convert(File file,targetDir) {
		println "bearbeite: ${file.name}";
		def xml = file.getText();
		def doc = new XmlParser().parseText(xml);
		def paragraphs = doc.paragraph;
		paragraphs.eachWithIndex {paragraph,index ->
			if((index == 0 || (index % 20) != 0) && index != paragraphs.size()-1 ) {
				paragraph.@type = "soft";
				def macroNode = paragraph.macro[0];
				if(macroNode != null) {
					paragraph.remove(macroNode);
				}
			}
		}
		def outFile = new File(targetDir + File.separatorChar + file.getName());
		def writer = outFile.newWriter("UTF-8");
		new XmlNodePrinter(new PrintWriter(writer)).print(doc);
		//		def result = writer.toString();
		//		println result
	}
}