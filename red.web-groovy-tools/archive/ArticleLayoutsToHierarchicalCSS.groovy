import javax.swing.text.SimpleAttributeSet
import eu.red_web.editor.attribute.EditorAttribute
import javax.swing.text.SimpleAttributeSet

import eu.red_web.commons.attributes.AttributeNames
import eu.red_web.editor.attribute.EditorAttribute
import eu.red_web.editor.css.StyleMappings
import eu.red_web.registry.embeddedserverimpl.ESInternalRegistry
import eu.red_web.server.embedded.EmbeddedServer
import eu.red_web.server.embedded.MasterControl
import eu.red_web.server.embedded.sync.ConfigurationDataSynchronisation
import eu.red_web.server.embedded.sync.SynchronisationException
import eu.red_web.server.services.wrapper.EmbeddedIOLayer
import eu.red_web.server.util.KeyGenerator
import groovy.sql.Sql


def server = "192.168.119.129"
def user = "hermesb"
def passwd = "hermesb"
def userHome = System.getProperty("user.home")
def enc = "UTF-8"

def sql = Sql.newInstance("jdbc:derby:${userHome}/.redweb/${server}/database/redweb30;create=false", "","", "org.apache.derby.jdbc.AutoloadedDriver")
def redwebHome = "${userHome}\\.redweb\\${server}\\filestore"

try {
  URL url = new URL("http://${server}:8080/server/dv?dvPort=8100")
  EmbeddedServer.startup(url)
  MasterControl.init()
  MasterControl.getInstance().connect(user,passwd,"null","null")
  EmbeddedIOLayer.startup(user)
  try {
    new ConfigurationDataSynchronisation().synchronize(MasterControl.getInstance().getForegroundFactory());
  } catch (SynchronisationException e) {
    e.printStackTrace()
  }
  new ESInternalRegistry().startup();
} catch (Exception  e) {
  e.printStackTrace()
}


def publicationsMap = [:]
def publicationsIDSQL = "SELECT RW_PUBLICATION.ID AS ID,RW_PUBLICATION.NAME AS NAME ,RW_CLIENT.NAME AS CLIENT FROM RW_PUBLICATION,RW_CLIENT WHERE RW_PUBLICATION.CLIENTID = RW_CLIENT.ID AND RW_PUBLICATION.NAME LIKE 'Tageszeitung'"
sql.eachRow(publicationsIDSQL) {
  publicationsMap[it.ID] = "${it.CLIENT}\\${it.NAME}"
}

int counter = 1

publicationsMap.each { publicationID,publicationClientAndName ->
  println "${publicationClientAndName}:"
  def piktoSrcDir = "${redwebHome}\\${publicationClientAndName}\\pictogram"
  def makroSrcDir = "${redwebHome}\\${publicationClientAndName}\\macro"

  def pictoSQL = "SELECT ID,NAME1,NAME2,NAME3,ACTIVE FROM RW_PICTOGRAM WHERE PUBLICATIONID LIKE ${publicationID} AND MARKEDFORDELETION = 0 AND ACTIVE=1"
  def pictoMap = [:]
  sql.eachRow(pictoSQL) {
    pictoMap[it.ID] = "${it.NAME1}_${it.NAME2}_${it.NAME3}".replaceAll("[\\/\\\\?\\%\\*\\:\\|\"\\<\\>=]", "_")
  }

  //[textbereich:5]
  def categoryCounter = [:]
  //[textbereich2:[attrSet]
  def attributesBycategoryWithCounter = [:]
  //[textbereich3:5]
  def categoryWithCounterUsage = [:] //counts usage of categoryWithCounter to decide if it's included by reference or used inside css definition
  //[aufmacher_24pt#textbereich:textbereich1,textbereich2,textbereich3]
  def attributesByCSSFileAndCategory = [:]
  //[cssFile1:[Dachzeile,Unterzeile,Textbereich]]
  def categoriesByCSSFile = [:]
  //[cssFile1,cssFile2,cssFile3,...]
  def cssFiles = []

  int debugStop = 10;
  try{
    new File(piktoSrcDir).eachFileMatch(~/.*\.xml/){ file->
      if(!file.name.startsWith("thumb")){
        if(true || debugStop-- > 0 ){
          println "Konvertiere ${file.name} ..."
          pictoID = file.name.replaceAll("\\.xml", "");
          if(pictoMap[pictoID] != null || pictoMap[pictoID].toString() != "null"){
            String cssFileName = pictoID //pictoMap[pictoID]
            cssFiles.add(cssFileName)
            def content = file.getText("UTF-8")
            def root = new XmlSlurper().parseText(content)
            def fields = root.depthFirst().findAll {
              it.name() == 'field' && it["@_type"] == "eu.red_web.layout.pictogram.model.area.PictoareaModel"
            };
            //parse categories
            fields.each{ field->
              def category = field.nameModel["@text"];
              def charAttributes = field.textAttributes.charAttributes.field;
              def paraAttributes = field.textAttributes.paragraphAttributes.field;
              println "Kategorie: ${category}"
              //create char and paragraph attributes for each category
              SimpleAttributeSet attrSet = new SimpleAttributeSet()
              charAttributes.each{
                def key = it['@key'].text() as String
                def type = it['@type'].text() as String
                def value = it['@value'].text() as String
                if(type.equals("java.lang.String")){
                  value = "\"${value}\""
                }
                def evalStr = "new ${type}(${value})"
                def typedVal = Eval.me(evalStr)
                EditorAttribute<?> editorAttr = new EditorAttribute<?>(typedVal)
                attrSet.addAttribute(key, editorAttr)
              }
              paraAttributes.each{
                def key = it['@key'].text() as String
                def type = it['@type'].text() as String
                def value = it['@value'].text() as String
                if(type.equals("java.lang.String")){
                  value = "\"${value}\""
                }
                def evalStr = null;
                if(type.equals("float[]")){
                  evalStr = "[${value}] as float[]"
                }else if(type.equals("eu.red_web.editor.text.initial.Initial") && !value.isEmpty()){
                  evalStr = "eu.red_web.editor.attribute.EditorAttributeFactory.createInitialAttribute(${value})"
                }else if(type.equals("eu.red_web.editor.text.tabs.RWTabSet") && !value.isEmpty()){
                  evalStr = "eu.red_web.editor.attribute.EditorAttributeFactory.createTabSetAttribute(${value})"
                }else{
                  evalStr = "new ${type}(${value})"
                }
                //    println "${evalStr}"
                def typedVal = Eval.me(evalStr);
                EditorAttribute<?> editorAttr = new EditorAttribute<?>(typedVal)
                attrSet.addAttribute(key, editorAttr)
              }
              def catStr = category.text() as String
              int similarity = Integer.MAX_VALUE
              SimpleAttributeSet simAttrSet = new SimpleAttributeSet();
              def simCategoryWithCounter = null;
              attributesBycategoryWithCounter.each{key,tempAttrSet->
                //println "${key} versus ${catStr}"
                if(key.startsWith(catStr)){
                  int currentSimilarity = detectSimilarity(attrSet,tempAttrSet)
                  //println "${cssFileName} key: ${key} similarity: ${currentSimilarity}}"
                  if(currentSimilarity < similarity){
                    similarity = currentSimilarity;
                    simAttrSet = tempAttrSet;
                    simCategoryWithCounter = key;
                  }
                }
              }
              def cssFileAndCategory = cssFileName + "#" + catStr
              attributesByCSSFileAndCategory[cssFileAndCategory] = [];
              if(simCategoryWithCounter != null){
                attributesByCSSFileAndCategory[cssFileAndCategory].add(simCategoryWithCounter);
                if(categoryWithCounterUsage[simCategoryWithCounter] == null){
                  categoryWithCounterUsage[simCategoryWithCounter] = 1;
                }else{
                  categoryWithCounterUsage[simCategoryWithCounter] = categoryWithCounterUsage[simCategoryWithCounter] + 1;
                }
              }
              SimpleAttributeSet diffAttrSet = diffAttributes(attrSet,simAttrSet)
              //if diffAttrset not empty create new categoryCounter
              if(!diffAttrSet.isEmpty()){
                def usedCategoryWithCounter = null
                //test if diffAttrSet is used before
                attributesBycategoryWithCounter.each{key,tempAttrSet->
                  //println "${key} versus ${catStr}"
                  if(usedCategoryWithCounter == null && key.startsWith(catStr)){
                    int currentSimilarity = detectSimilarity(diffAttrSet,tempAttrSet)
                    if(currentSimilarity == 0){
                      usedCategoryWithCounter = key
                    }
                  }
                }
                if(usedCategoryWithCounter != null){
                  attributesByCSSFileAndCategory[cssFileAndCategory].add(usedCategoryWithCounter);
                  if(categoryWithCounterUsage[usedCategoryWithCounter] == null){
                    categoryWithCounterUsage[usedCategoryWithCounter] = 1;
                  }else{
                    categoryWithCounterUsage[usedCategoryWithCounter] = categoryWithCounterUsage[usedCategoryWithCounter] + 1;
                  }
                }else {
                  if(categoryCounter[catStr] == null){
                    categoryCounter[catStr] = 1;
                  }else{
                    categoryCounter[catStr] = categoryCounter[catStr]+1;
                  }
                  def categoryWithCounter = catStr + categoryCounter[catStr];
                  attributesBycategoryWithCounter[categoryWithCounter] = diffAttrSet;
                  attributesByCSSFileAndCategory[cssFileAndCategory].add(categoryWithCounter);
                  if(categoryWithCounterUsage[categoryWithCounter] == null){
                    categoryWithCounterUsage[categoryWithCounter] = 1;
                  }else{
                    categoryWithCounterUsage[categoryWithCounter] = categoryWithCounterUsage[categoryWithCounter] + 1;
                  }
                }
              }
              if(categoriesByCSSFile[cssFileName] == null){
                categoriesByCSSFile[cssFileName] = []
              }
              categoriesByCSSFile[cssFileName].add(catStr)
            }
          }
        }
      }
    }
  }catch(Exception e){
    e.printStackTrace()
  }
  def cssMap = [:]
  def tempFileList = []
  def marker = "/*end includes*/"
  cssFiles.each{fileName->
    println "generate content ${fileName}"
    StringBuffer cssContent = new StringBuffer(marker)
    def categories = categoriesByCSSFile[fileName]
    categories.each{category->
      def key = fileName + "#" + category
      def categoriesWithCounter = attributesByCSSFileAndCategory[key]
      println "categoriesWithCounter: ${categoriesWithCounter}"
      categoriesWithCounter.each{categoryWithCounter->
        println "categoryWithCounter: ${categoryWithCounter}"
        def usageCount = categoryWithCounterUsage[categoryWithCounter]
        if(usageCount < 2){
          SimpleAttributeSet attrSet = attributesBycategoryWithCounter[categoryWithCounter]
          //println "attrSet getAttributeCount(): ${attrSet.getAttributeCount()}"
          def cssDef = StyleMappings.getInstance().attributeToCss(publicationID, attrSet,false).replaceAll(";", ";\n\t")
          cssContent.append("\n.${category}{\n");
          cssContent.append("\t${cssDef}");
          cssContent.append("\n}\n\n");
        }else{
          def index = cssContent.indexOf(marker)
          def impFileName = categoryWithCounter; // + ".css"
          def includeStr = "@import \"${impFileName}\";\n"
          cssContent.insert(index, includeStr)
          if(!tempFileList.contains(impFileName)){
            tempFileList.add(impFileName)
            SimpleAttributeSet attrSet = attributesBycategoryWithCounter[categoryWithCounter]
            def cssDef = StyleMappings.getInstance().attributeToCss(publicationID, attrSet,true).replaceAll(";", ";\n\t")
            StringBuffer impCssContent = new StringBuffer(marker)
            impCssContent.append("\n.${category}{\n");
            impCssContent.append("\t${cssDef}");
            impCssContent.append("\n}\n\n");
            cssMap[impFileName] = impCssContent
          }
        }
      }
    }
    //def tmpFileName = "${fileName}"
    def existingFileName = null;
    String tmpContent = cssContent.toString()
    cssMap.each{k,v->
      if(v.equals(tmpContent.toString())){
        existingFileName = k;
        return;
      }
    }
    if(existingFileName == null){
      cssMap[fileName] = tmpContent;
    }else{
      cssMap.remove(existingFileName);
      cssMap["${fileName}-_-${existingFileName}"] = tmpContent;
    }
  }

  def labelCSSID = KeyGenerator.createUniqueKey();
  println "labelCSSID: ${labelCSSID}"
  File labelCSSFile = new File("d:\\temp\\cssArticleLayouts\\${labelCSSID}.css");
  labelCSSFile.write("",enc);

  def styleByFile = [:]
  cssMap.each{fileName,cssContent ->
    println "write file: ${fileName}"
    def styleID = styleByFile[fileName]
    if(styleID == null){
      styleID = KeyGenerator.createUniqueKey();
      styleByFile[fileName] = styleID
    }
    File cssFile = new File("d:\\temp\\cssArticleLayouts\\${styleID}.css");
    cssFile.write("/*start picto ids\n",enc);
    fileName.split("-_-").each{pictoID->
      if(pictoMap[pictoID] != null || pictoMap[pictoID].toString() != "null"){
        cssFile.append(pictoID + "\n",enc);
      }
    }
    cssFile.append("end picto ids*/\n",enc);

    def cssImportFileNames = cssContent.toString()
    def endInclStr = "/*end includes*/";
    def endInclIndex = cssImportFileNames.indexOf(endInclStr)
    def newImpHeader = ""
    if(endInclIndex > 0){
      def cssImports = cssImportFileNames.substring(0, endInclIndex)
      def tempEndIncl = endInclIndex + endInclStr.length() + 1 < cssImportFileNames.length() ? endInclIndex + endInclStr.length() + 1 : endInclIndex + endInclStr.length();
      cssImportFileNames = cssImportFileNames.substring(tempEndIncl)
      println "cssImportFileNames: ${cssImportFileNames}"
      cssImports.split("\n").each{importStr->
        if(importStr.length() > 0){
          def firstP = importStr.indexOf("\"")
          if(firstP >= 0 && firstP+1 < importStr.length()){
            def secP = importStr.indexOf("\"",firstP+1)
            if(secP >= 0){
              def impName = importStr.substring(firstP+1,secP)
              def impStyleID = styleByFile[impName]
              if(impStyleID == null){
                impStyleID = KeyGenerator.createUniqueKey();
                styleByFile[impName] = impStyleID
              }
              //println "impName: ${impName} impStyleID: ${impStyleID}"
              def modImpLine = importStr.substring(0,firstP+1) + impStyleID + "\";\n"
              //println "impName: ${impName} impStyleID: ${impStyleID} modImpLine: ${modImpLine}"
              newImpHeader += modImpLine
            }
          }
        }
      }
      newImpHeader += "@import \"${labelCSSID}\";\n"
    }
    cssFile.append(newImpHeader + "\n" + cssImportFileNames,enc)
  }
}

static int detectSimilarity(SimpleAttributeSet set1,SimpleAttributeSet set2){
  int diffCount = 0
  def attrNames = []
  attrNames.addAll(AttributeNames.ALL_CA_ATTR_NAMES)
  attrNames.addAll(AttributeNames.ALL_PA_ATTR_NAMES)
  attrNames.each{attrName->
    def set1Val = set1.getAttribute(attrName);
    def set2Val = set2.getAttribute(attrName);
    if(set1Val != null){
      if(!set1Val.equals(set2Val)){
        diffCount++
      }
    }else{
      if(set2Val != null){
        diffCount++
      }
    }
  }
  if(diffCount < 5){
    return diffCount
  }
  return Integer.MAX_VALUE
}


static SimpleAttributeSet diffAttributes(SimpleAttributeSet set1,SimpleAttributeSet set2){
  SimpleAttributeSet diffSet = new SimpleAttributeSet()
  def attrNames = []
  attrNames.addAll(AttributeNames.ALL_CA_ATTR_NAMES)
  attrNames.addAll(AttributeNames.ALL_PA_ATTR_NAMES)
  attrNames.each{attrName->
    def set1Val = set1.getAttribute(attrName);
    def set2Val = set2.getAttribute(attrName);
    if(set1Val != null){
      if(!set1Val.equals(set2Val)){
        diffSet.addAttribute(attrName, set1Val)
      }
    }else{
      if(set2Val != null){
        diffSet.addAttribute(attrName, set2Val)
      }
    }
  }
  return diffSet
}

