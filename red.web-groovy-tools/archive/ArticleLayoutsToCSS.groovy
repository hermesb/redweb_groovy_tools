import javax.swing.text.SimpleAttributeSet

import eu.red_web.editor.attribute.EditorAttribute
import eu.red_web.editor.css.StyleMappings
import eu.red_web.registry.embeddedserverimpl.ESInternalRegistry
import eu.red_web.server.embedded.EmbeddedServer
import eu.red_web.server.embedded.MasterControl
import eu.red_web.server.embedded.sync.ConfigurationDataSynchronisation
import eu.red_web.server.embedded.sync.SynchronisationException
import eu.red_web.server.services.wrapper.EmbeddedIOLayer
import groovy.sql.Sql


def server = "redweb-t.it4media.de"
def user = "root"
def passwd = "itoito"
def userHome = System.getProperty("user.home")
def enc = "ISO-8859-1"

def sql = Sql.newInstance("jdbc:derby:${userHome}/.redweb/${server}/database/redweb30;create=false", "","", "org.apache.derby.jdbc.AutoloadedDriver")
def redwebHome = "${userHome}\\.redweb\\${server}\\filestore"

try {
  URL url = new URL("http://${server}:8080/server/dv?dvPort=8100")
  EmbeddedServer.startup(url)
  MasterControl.init()
  MasterControl.getInstance().connect(user,passwd,"null","null")
  EmbeddedIOLayer.startup(user)
  try {
    new ConfigurationDataSynchronisation().synchronize(MasterControl.getInstance().getForegroundFactory());
  } catch (SynchronisationException e) {
    e.printStackTrace()
  }
  new ESInternalRegistry().startup();
} catch (Exception  e) {
  e.printStackTrace()
}


def publicationsMap = [:]
def publicationsIDSQL = "SELECT RW_PUBLICATION.ID AS ID,RW_PUBLICATION.NAME AS NAME ,RW_CLIENT.NAME AS CLIENT FROM RW_PUBLICATION,RW_CLIENT WHERE RW_PUBLICATION.CLIENTID = RW_CLIENT.ID AND RW_PUBLICATION.NAME LIKE 'Tageszeitung'"
sql.eachRow(publicationsIDSQL) {
  publicationsMap[it.ID] = "${it.CLIENT}\\${it.NAME}"
}

int counter = 1

publicationsMap.each { publicationID,publicationClientAndName ->
  println "${publicationClientAndName}:"
  def piktoSrcDir = "${redwebHome}\\${publicationClientAndName}\\pictogram"
  def makroSrcDir = "${redwebHome}\\${publicationClientAndName}\\macro"

  def pictoSQL = "SELECT ID,NAME1,NAME2,NAME3,ACTIVE FROM RW_PICTOGRAM WHERE PUBLICATIONID LIKE ${publicationID} AND MARKEDFORDELETION = 0 AND ACTIVE=1"
  def pictoMap = [:]
  sql.eachRow(pictoSQL) {
    pictoMap[it.ID] = "${it.NAME1}_${it.NAME2}_${it.NAME3}".replaceAll("[\\/\\\\?\\%\\*\\:\\|\"\\<\\>=]", "_")
  }

  try{
    new File(piktoSrcDir).eachFileMatch(~/.*\.xml/){ file->
      if(!file.name.startsWith("thumb")){
        println "Konvertiere ${file.name} ..."
        pictoID = file.name.replaceAll("\\.xml", "");
        if(pictoMap[pictoID] != null || pictoMap[pictoID].toString() != "null"){
          File cssFile = new File("d:\\temp\\cssArticleLayouts\\${pictoMap[pictoID]}.css");
          cssFile.write("/*\n${pictoID}\n${pictoMap[pictoID]}\n*/\n",enc);
          def content = file.getText("UTF-8")
          def root = new XmlSlurper().parseText(content)
          def fields = root.depthFirst().findAll {
            it.name() == 'field' && it["@_type"] == "eu.red_web.layout.pictogram.model.area.PictoareaModel"
          };
          fields.each{ field->
            def category = field.nameModel["@text"];
            def charAttributes = field.textAttributes.charAttributes.field;
            def paraAttributes = field.textAttributes.paragraphAttributes.field;
            //println "Kategorie: ${category}"
            SimpleAttributeSet charAttrSet = new SimpleAttributeSet()
            charAttributes.each{
              def key = it['@key'].text() as String
              def type = it['@type'].text() as String
              def value = it['@value'].text() as String
              if(type.equals("java.lang.String")){
                value = "\"${value}\""
              }
              def evalStr = "new ${type}(${value})"
              def typedVal = Eval.me(evalStr)
              EditorAttribute<?> editorAttr = new EditorAttribute<?>(typedVal)
              charAttrSet.addAttribute(key, editorAttr)
            }
            SimpleAttributeSet paraAttrSet = new SimpleAttributeSet()
            paraAttributes.each{
              def key = it['@key'].text() as String
              def type = it['@type'].text() as String
              def value = it['@value'].text() as String
              if(type.equals("java.lang.String")){
                value = "\"${value}\""
              }
              def evalStr = null;
              if(type.equals("float[]")){
                evalStr = "[${value}] as float[]"
              }else if(type.equals("eu.red_web.editor.text.tabs.RWTabSet") && !value.isEmpty()){
                evalStr = "eu.red_web.editor.attribute.EditorAttributeFactory.createTabSetAttribute(${value})"
              }else{
                evalStr = "new ${type}(${value})"
              }
              //    println "${evalStr}"
              def typedVal = Eval.me(evalStr);
              EditorAttribute<?> editorAttr = new EditorAttribute<?>(typedVal)
              paraAttrSet.addAttribute(key, editorAttr)
            }
            def charCSS = StyleMappings.getInstance().attributeToCss(publicationID, charAttrSet,true).replaceAll(";", ";\n\t")
            def paraCSS = StyleMappings.getInstance().attributeToCss(publicationID, paraAttrSet,true).replaceAll(";", ";\n\t")
            cssFile.append("\n.${category}{\n", enc);
            cssFile.append("\t${charCSS}",enc);
            cssFile.append(paraCSS,enc);
            cssFile.append("\n}\n\n");
          }
        }
      }
    }
  }catch(Exception e){
    e.printStackTrace()
  }
}
