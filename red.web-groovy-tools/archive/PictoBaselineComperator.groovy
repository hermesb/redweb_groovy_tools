import java.io.File;


class PictoComperator{
	
	static void main(args) {
		//		def srcDir = /d:\backup\me-test\ME\pictogram/;
		//		def targetDir = /c:\Users\hermes\.redweb\172.16.10.120\filestore\Main-Echo\ME\pictogram/;
		def srcDir = /d:\temp\mrv-werte\pictogram/;
		def targetDir = /c:\Users\hermes\.redweb\produktion3.red-web.org\filestore\Mittelrhein-Verlag\Tageszeitung\pictogram/;
		def file = new File(srcDir).eachFileMatch(~/.*\.xml/){file->
			println "lese: ${file.name}";
			def result1 = "";
			def xml = file.getText();
			def doc = new XmlParser().parseText(xml);
			def styles = doc.textproperties.pictoarea.text.styles;
			styles.style.attribute.each{attribute->
				def key = attribute.@key;
				if(key.equals("pa:BaselineOffset")) {
					result1 += attribute.@value + "_";
				}
			}
			File targetFile = null;
			try {
				targetFile = new File(targetDir + File.separatorChar + file.name);
				//println "lese: ${targetFile}";
				def result2 = "";
				xml = targetFile.getText();
				doc = new XmlParser().parseText(xml);
				styles = doc.textproperties.pictoarea.text.styles;
				styles.style.attribute.each{attribute->
					def key = attribute.@key;
					if(key.equals("pa:BaselineOffset")) {
						result2 += attribute.@value + "_";
					}
				}
				println "Vergleich ${result1.equals(result2)} ${result1} + ${result2}";
			}catch (Exception e) {
				println "Konnte Datei nicht lesen: ${targetFile}";
			}
		}
	}
}