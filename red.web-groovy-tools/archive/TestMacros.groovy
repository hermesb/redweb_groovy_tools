import javax.swing.text.SimpleAttributeSet

import org.apache.commons.lang.StringEscapeUtils

import eu.red_web.commons.attributes.AttributeNames
import eu.red_web.commons.fonts.model.FontModel
import eu.red_web.commons.io.IOLayer
import eu.red_web.editor.attribute.EditorAttribute
import eu.red_web.editor.attribute.EditorAttributeFactory
import eu.red_web.editor.editorkit.view.RWTextPane
import eu.red_web.editor.scripting.ScriptingHost
import eu.red_web.editor.scripting.ScriptingHost.EvaluationResult
import eu.red_web.editor.text.TextController
import eu.red_web.editor.text.TextControllerImpl
import eu.red_web.registry.RedwebRegistry
import eu.red_web.registry.embeddedserverimpl.ESInternalRegistry
import eu.red_web.server.embedded.EmbeddedServer
import eu.red_web.server.embedded.MasterControl
import eu.red_web.server.embedded.sync.ConfigurationDataSynchronisation
import eu.red_web.server.embedded.sync.SynchronisationException
import eu.red_web.server.services.wrapper.EmbeddedIOLayer
import groovy.sql.Sql


import javax.swing.text.SimpleAttributeSet

import org.apache.commons.lang.StringEscapeUtils

import eu.red_web.commons.attributes.AttributeNames
import eu.red_web.commons.fonts.model.FontModel
import eu.red_web.commons.io.IOLayer
import eu.red_web.editor.attribute.EditorAttribute
import eu.red_web.editor.attribute.EditorAttributeFactory
import eu.red_web.editor.editorkit.view.RWTextPane
import eu.red_web.editor.scripting.ScriptingHost
import eu.red_web.editor.scripting.ScriptingHost.EvaluationResult
import eu.red_web.editor.text.TextController
import eu.red_web.editor.text.TextControllerImpl
import eu.red_web.registry.RedwebRegistry
import eu.red_web.registry.embeddedserverimpl.ESInternalRegistry
import eu.red_web.server.embedded.EmbeddedServer
import eu.red_web.server.embedded.MasterControl
import eu.red_web.server.embedded.sync.ConfigurationDataSynchronisation
import eu.red_web.server.embedded.sync.SynchronisationException
import eu.red_web.server.services.wrapper.EmbeddedIOLayer
import groovy.sql.Sql

//def server = "neptun.red-web.lan"
def server = "me-rw-prod.me-intern.de"
def user = "hermesb"
def passwd = "hermesb"
def userHome = System.getProperty("user.home");


def enc = "UTF-8";
def reportFile = new File("report-${server}.html");
reportFile.write("""\
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <link type="text/css" href="css/smoothness/jquery-ui-1.8.7.custom.css" rel="stylesheet" />
  <link type="text/css" href="css/report.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/jquery.snippet.min.css" />
  <script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui-1.8.7.custom.min.js"></script>
  <script type="text/javascript" src="js/jquery.snippet.min.js"></script>
  <script>
    \$(document).ready(function(){
      
    \$("pre.htmlCode").snippet("html");
      // Finds <pre> elements with the class "htmlCode"
      // and snippet highlights the HTML code within.
     
    \$("pre.styles").snippet("css",{style:"greenlcd"});
      // Finds <pre> elements with the class "styles"
      // and snippet highlights the CSS code within
      // using the "greenlcd" styling.
     
    \$("pre.js").snippet("javascript",{style:"ide-eclipse",transparent:true,showNum:true});
      // Finds <pre> elements with the class "js"
      // and snippet highlights the JAVASCRIPT code within
      // using a random style from the selection of 39
      // with a transparent background
      // without showing line numbers.
     });
     
  </script>
</head>
<body>
<ul>""",enc);

def sql = Sql.newInstance("jdbc:derby:${userHome}/.redweb/${server}/database/redweb30;create=false", "","", "org.apache.derby.jdbc.AutoloadedDriver")
def redwebHome = "${userHome}\\.redweb\\${server}\\filestore"

try {
  URL url = new URL("http://${server}:8080/server/dv?dvPort=8100");
  EmbeddedServer.startup(url);
  MasterControl.init();
  MasterControl.getInstance().connect(user,passwd,"null","null");
  EmbeddedIOLayer.startup(user);
  try {
    new ConfigurationDataSynchronisation().synchronize(MasterControl.getInstance().getForegroundFactory());
  } catch (SynchronisationException e) {
    e.printStackTrace();
  }
  new ESInternalRegistry().startup();
} catch (Exception  e) {
  e.printStackTrace();
}

def publicationsMap = [:]
def publicationsIDSQL = "SELECT RW_PUBLICATION.ID AS ID,RW_PUBLICATION.NAME AS NAME ,RW_CLIENT.NAME AS CLIENT FROM RW_PUBLICATION,RW_CLIENT WHERE RW_PUBLICATION.CLIENTID = RW_CLIENT.ID"
sql.eachRow(publicationsIDSQL) {
  publicationsMap[it.ID] = "${it.CLIENT}\\${it.NAME}"
}
int counter = 1;

publicationsMap.each { publicationID,publicationClientAndName ->

  println "${publicationClientAndName}:"

  def makroSrcDir = "${redwebHome}\\${publicationClientAndName}\\macro"
  def macroSQL = "SELECT ENTITYID,NAME,ACTIVE,MACROGROUP FROM NAME,RW_MACRO WHERE PUBLICATIONID LIKE ${publicationID} AND NAME.ENTITYID = RW_MACRO.ID AND MARKEDFORDELETION = 0"
  def macroGroup = [:]
  def macroName = [:]
  def macroState = [:]
  sql.eachRow(macroSQL) {
    macroGroup[it.ENTITYID] = "${publicationsMap[publicationID]}\\${it.MACROGROUP}"
    macroName[it.ENTITYID] = it.NAME
    macroState[it.ENTITYID] = it.ACTIVE
  }

  def errorMap = [:];
  try{
    new File(makroSrcDir).eachFileMatch(~/.*\.js/){file->
      String macroID = file.name.toString().substring(0, file.name.toString().length()-3);
      String source = file.getText();
      println "Teste Makro ${macroGroup[macroID]}#${macroName[macroID]}"
      TextController textController = null;
      try{
        textController = new TextControllerImpl();
        RWTextPane textPane = new RWTextPane();
        textPane.setDocument(textController.getModel());
        textController.setTextPane(textPane);
        textController.getTextPane().setPublicationID(publicationID);
        initTextController(textController,publicationID)
      }catch(Exception e){
        println "error while initializing textcontroller: ${e.getMessage()}";
      }
      try{
        int startOffset = textController.getStartOffsetForCategory("Textbereich")
        textController.setCaretPosition(startOffset)
      }catch(Exception e){
        println "erro while setting caret position: ${e.getMessage()}";
        println "Text:\n${textController.getText()}"
      }
      EvaluationResult result = ScriptingHost.getInstance().evaluateScript(source,textController);
      if(result.failed){
        errorMap[macroID] = [result, source];
      }
    }
  }catch(Exception e){
    println e.getMessage();
  }
  if(errorMap.size() > 0){
    reportFile.append("\n<li class=\"publication\">${StringEscapeUtils.escapeHtml(publicationClientAndName)}\n",enc);
    reportFile.append("\n<ul>");
    errorMap.each{id,value->
      def result = value[0]
      def source = value[1]
      println "Makro ${macroGroup[id]}#${macroName[id]} id: ${id} failed:\n error at ${result.line}: ${result.details}"
      reportFile.append("\n<li class=\"macro\">",enc);
      reportFile.append("\n<p class=\"active${macroState[id]}\">${macroGroup[id]}: ${macroName[id]} <span>${id}</span></P>",enc);
      reportFile.append("\n<div class=\"errorDescription\">Fehler in Zeile ${result.line}: ${StringEscapeUtils.escapeHtml(result.details)}</div",enc)
      reportFile.append("\n<div class=\"source\"><pre class=\"js\">${StringEscapeUtils.escapeHtml(source)}</pre></div>")
      reportFile.append("\n</li>",enc);
    }
    reportFile.append("\n</ul>");
  }else{
  }

  reportFile.append("\n</li>",enc);
}
reportFile.append("\n</ul></body></html>",enc);



static void initTextController(TextController textController,String publicationID){
  def categories = [
    "Leiste",
    "Dachzeile",
    "Hauptüberschrift",
    "Unterzeile",
    "Vorspann",
    "Textbereich"
  ]

  categories.each {category->
    SimpleAttributeSet paAttr = new SimpleAttributeSet();
    SimpleAttributeSet caAttr = new SimpleAttributeSet();
    initDefaultFontAndColor(caAttr,publicationID);
    textController.initDefaultAttr(category, caAttr, null, paAttr, null);
    EditorAttribute<String> categoryAttr = EditorAttributeFactory.createStringAttribute(category);
    paAttr.addAttribute(AttributeNames.PA_CATEGORY, categoryAttr);
    textController.appendParagraph(paAttr, caAttr, category.equals("Textbereich"));
  }
  categories.each {category->
    int startOffset = textController.getStartOffsetForCategory(category)
    if(!category.equals("Textbereich")){
      textController.insertString(startOffset, "${category}: bla bla bla")
    }else{
      def testText = """\
Microsoft hat mit Webmatrix ein kostenloses Paket zur Entwicklung von Webapplikationen unter Windows veröffentlicht.
Es enthält neben einer einfachen Entwicklungsumgebung auch einen Web- und Datenbankserver.
Das im Juli 2010 erstmals in einer Vorabversion veröffentlichte Entwicklerpaket Webmatrix steht ab sofort in einer finalen Version zum Download bereit.
Es umfasst den Webserver IIS Developer Express, den Datenbankserver SQL Server Compact sowie das Webframework ASP.Net.
Hinzu kommt ein integrierter Code- und Datenbankeditor.
Gedacht ist das Paket für Webentwicklungseinsteiger. Um einfache Projekte umzusetzen, soll der Anwender nur wenige Konzepte erlernen müssen.
Dabei lassen sich neue Webseiten auf Basis einer bestehenden Open-Source-Applikation aus einer Applikationsgalerie, aus bestehenden Templates oder einem leeren Ordner erstellen.
Webmatrix kann unter microsoft.com/web/webmatrix heruntergeladen werden."""
      textController.insertString(startOffset,testText)
    }
  }
}

static void initDefaultFontAndColor(SimpleAttributeSet caAttr,String publicationID) {
  String fontId = null;
  String fgColorId = null;
  try{

    fontId = IOLayer.getInstance().getFontInterface().getDefaultFontID(publicationID);
    if (fontId == null || fontId.length() == 0){
      Vector<FontModel> fontModels = RedwebRegistry.getInstance().getRedwebFontRegistry().getFonts(publicationID, true);
      if(fontModels.size() > 0){
        fontId = fontModels.get(0).getId();
      }else{
        fontId = "0123456789012345"
      }
    }
    fgColorId = RedwebRegistry.getInstance().getRedwebColor().getSystemBlack(publicationID).getId();
  }catch (Exception e) {
    e.printStackTrace();
  }
  if(fontId != null){
    caAttr.addAttribute(AttributeNames.CA_FONT, EditorAttributeFactory.createStringAttribute(fontId));
  }
  if(fgColorId != null){
    caAttr.addAttribute(AttributeNames.CA_FONT_COLOR, EditorAttributeFactory.createStringAttribute(fgColorId));
  }
}
