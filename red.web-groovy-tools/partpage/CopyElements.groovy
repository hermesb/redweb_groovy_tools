package partpage

import org.apache.log4j.ConsoleAppender
import org.apache.log4j.DailyRollingFileAppender
import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.apache.log4j.PatternLayout

import distantvoices3.client.BasicClient
import eu.red_web.server.client.ClientFactory
import eu.red_web.server.services.TypeMapper

def eTag = null
def cli = new CliBuilder()
cli.with {
     usage: 'Self'
     h longOpt:'help', 'usage information'
     e longOpt:'etag', 'ETag (Format yyyy-MM-dd)', args:1
}
def opt = cli.parse(args)

if( opt.h ) {
    cli.usage()
    return
}
if( opt.e ) {
  eTag = opt.e
}

Logger log = Logger.getRootLogger();
log.removeAllAppenders();
ConsoleAppender appender1 = new ConsoleAppender(new PatternLayout("%d{ISO8601} %-5p [%c]: %m%n"));
DailyRollingFileAppender appender2 = new DailyRollingFileAppender(new PatternLayout("%d{ISO8601} %-5p [%c]: %m%n"),"titelseiten.log","yyyy-MM-dd")
log.addAppender(appender1);
log.addAppender(appender2);
log.setLevel(Level.INFO);


def day = ""
if(eTag == null){
  Date date = new Date(System.currentTimeMillis())
  day = date.format("EE")
  date = date.plus(4);
  eTag = date.format("yyyy-MM-dd")
  day = date.format("EE")
}else{
  Date date = Date.parse("yyyy-MM-dd", eTag)
  day = date.format("EE")
}

log.info("Etag: ${eTag} Tag: ${day}")


def server = "redweb.zld.name"
def user = "hermesb"
def passwd = "hermesb"
//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, 8100, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null){
  throw new Exception(keys[3])
}
cl.setPassword(keys[1])

action(f,eTag,day,log)

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  log.error("Fehler w�hrend des Logouts: ${e.getMessage()}")
}



def action(ClientFactory f,eTag,day,Logger log){
  def blueTitlePages = ["111","113","121","122","123","131","141","142"]
  def orangeTitlePages = ["124","125","126"]
  def allTitlePages = blueTitlePages + orangeTitlePages
  try{
    def limi_ext = day.equalsIgnoreCase("di") || day.equalsIgnoreCase("fr") ?  "_${day.toLowerCase()}" : ""
    def sourcePartPageList = ["Seitenmuster_titelhinweise_limi${limi_ext}":allTitlePages,"Seitenmuster_titelhinweise_re_111":blueTitlePages,"Seitenmuster_titelhinweise_re_124":orangeTitlePages];

    sourcePartPageList.each{sourcePartPageName,targetPartPagePrefixList->
      def targetPartPageName = "${targetPartPagePrefixList.head()}_ts_1"

      def sourcePartPageID = searchPartPage(f,sourcePartPageName,null,log)
      def targetPartPageID = searchPartPage(f,targetPartPageName,eTag,log)

      def sourceElements = getSourceElementsWithPosition(f,sourcePartPageID,log)
      if(targetPartPageID != null && !targetPartPageID.isEmpty()){
        def copiedSourcElements = copySourceElementsToTarget(f,targetPartPageID,sourceElements,false,eTag,day,log)
        targetPartPagePrefixList.tail().each{
          def targetLinkedPartPageName = "${it}_ts_1"
          targetPartPageID = searchPartPage(f,targetLinkedPartPageName,eTag,log)
          if(targetPartPageID != null && !targetPartPageID.isEmpty()){
            copySourceElementsToTarget(f,targetPartPageID,copiedSourcElements,true,eTag,day,log)
          }else{
            log.info("Zielteilseite: ${targetLinkedPartPageName} wurde nicht gefunden")
          }
        }
      }else{
        log.info("Zielteilseite: ${targetPartPageName} wurde nicht gefunden")
      }
    }
  }catch(Exception e){
    e.printStackTrace()
  }
}


def searchPartPage(ClientFactory f,String name,String eTag,Logger log){
  def id = null;
  def query = ""
  if(eTag != null && !eTag.isEmpty()){
    query = """
      <query>
          <select>partpage.id</select>
          <where>
            <and>
              <field name=\"partpage.keyword\" comparator=\"equal\" value=\"${name}\"/>
              <field name=\"partpage_issues.date\" comparator=\"equal\" value=\"${eTag}\"/>
            </and>
          </where>
      </query>
      """
  }else{
    query = """
      <query>
          <select>partpage.id</select>
          <where>
            <field name=\"partpage.keyword\" comparator=\"equal\" value=\"${name}\"/>
          </where>
      </query>
      """
  }
  log.info("Suche nach der Teilseite ${name} mit ETag: ${eTag}")

  try {
    f.getTransactionDVProxy().open()
    def resultStr = f.getPartPageDVProxy().search(query, false)
    f.getTransactionDVProxy().commit()

    def result = new XmlSlurper().parseText(resultStr);
    id = result.row.partpage.field.@value.text(); //.collect{it.text()}
  } catch (Exception e) {
    log.error("Fehler w�hrend der Suche der Teilseite ${name}: ${e.getMessage()}")
    try {
      f.getTransactionDVProxy().rollback()
    } catch (Exception e2) {
      log.error("W�hrend des Zu�rckspielens der Transaktion ist ein Fehler aufgetreten: ${e2.getMessage()}")
    }
  }
  return id;
}

def getSourceElementsWithPosition(ClientFactory f, String sourceID,Logger log){
  def sourceIDs = []
  try {
    f.getTransactionDVProxy().open()
    String[][] placedElements = f.getPartPageDVProxy().getPlacedElements(sourceID)
    placedElements.each {
      def type = it[0]
      def id = it[1]
      long[] position = f.getPartPageDVProxy().getElementPosition(sourceID,id)
      sourceIDs.add([type,id,position[0],position[1]]);
    }
    f.getTransactionDVProxy().commit()
  } catch (Exception e) {
    log.error("Fehler bei der Ermittlung der Quellelemente von der Teilseite ${sourceID}: ${e.getMessage()}")
    try {
      f.getTransactionDVProxy().rollback()
    } catch (Exception e2) {
      log.error("W�hrend des Zu�rckspielens der Transaktion ist ein Fehler aufgetreten: ${e2.getMessage()}")
    }
  }
  return sourceIDs
}

def copySourceElementsToTarget(ClientFactory f,String targetID,sourceElements,boolean linkElements,String eTag,String day,Logger log){
  def copiedSourceIDS = []
  //log.info("Kopiere auf die Teilseite ${targetID} folgende Elemente ${sourceElements}")
  def lockResult = null
  try{
    lockResult = f.getPartPageDVProxy().testAndLock(targetID)
  }catch(Exception e){
    log.error("W�hrend des Sperrens der Teilseite ${targetID} ist ein Fehler aufgetreten")
  }
  if(lockResult == null || lockResult[0].equalsIgnoreCase("false")){
    log.info("Die Teilseite ${targetID} konnte nicht gesperrt werden. Es k�nnen keine Elemente auf diese Teilseite kopiert oder verlinkt werden")
    return copiedSourceIDS
  }
  log.info("Die Teilseite ${targetID} ist jetzt gesperrt")
  sourceElements.each{
    try {
      f.getTransactionDVProxy().open()
      String type = it[0]
      String id = it[1]
      int x = it[2]
      int y = it[3]
      String[][] keywordMap = [[]] as String[][];
      def copyID = "" 
      def dvProxy = TypeMapper.getProxyByType(f, type);
      def keyword = dvProxy.getKeyword(id);
      def flag = true
//      if(keyword.toLowerCase().endsWith("nur_di") && !day.equalsIgnoreCase("di")){
//        flag = false
//      }
//      if(keyword.toLowerCase().endsWith("nur_fr") && !day.equalsIgnoreCase("fr")){
//        flag = false
//      }
      if(flag == true){
        if(linkElements){
          copyID = id
          //println "Verlinke Element: ${id}"
        }else{
          Date date = new Date(System.currentTimeMillis())
          def dateStr = date.format("yyyy-MM-dd")
          copyID = dvProxy.copy(id, true, "_${dateStr}")
          //println "Kopiere Element: ${id} to ${copyID} extension: ${dateStr}"
        }
        log.info("F�ge ${type} (${copyID}) auf die Teilseite ${targetID} als ${linkElements ? 'Verlinkung' : 'Kopie'}")
        f.getPartPageDVProxy().addElement(targetID, copyID, x, y)
        if(type.equalsIgnoreCase("article")){
          f.getPartPageDVProxy().setElementDisplaces(targetID,copyID, false)
          f.getPartPageDVProxy().setElementDisplacesText(targetID,copyID, false)
        }
        copiedSourceIDS.add([type,copyID,x,y]);
      }
      f.getTransactionDVProxy().commit()
    } catch (Exception e) {
      log.error("W�hrend des Kopierens der Elemente auf die Teilseite ${targetID} ist ein Fehler aufgetreten: ${e.getMessage()}")
      try {
        f.getTransactionDVProxy().rollback()
      } catch (Exception e2) {
        log.error("W�hrend des Zu�rckspielens der Transaktion ist ein Fehler aufgetreten: ${e2.getMessage()}")
      }
    }
  }
  try{
    f.getPartPageDVProxy().unlock(targetID)
  }catch(Exception e){
    log.error("W�hrend der Freigabe der Teilseite ${targetID} ist ein Fehler aufgetreten")
  }
  log.info("Die Teilseite ${targetID} ist jetzt freigegeben")
  return copiedSourceIDS 
}