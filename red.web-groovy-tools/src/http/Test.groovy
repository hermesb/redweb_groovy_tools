package http

import groovy.json.JsonSlurper

def server = "sunrise.red-web.lan"
def port = 8080
def dvPort = 8100
def user = "hermesb"
def passwd = "hermesb"

def service = "MetaService"
def function = "getServerVersion"
def args = ''
def urlstr = "http://${server}:${port}/server/dv/rest/${dvPort}/${service}/${function}?${args}"

callService(urlstr)

//LOGIN
service = "SessionService"
function = "login"
args = "arg0=%22hermesb%22&arg1=null&arg2=%22hermesb%22&arg3=null"
urlstr = "http://${server}:${port}/server/dv/rest/${dvPort}/${service}/${function}?${args}"
println urlstr
def result = callService(urlstr)


def callService(urlstr){
  def url = new URL( urlstr)
  def result = url.text
  def jsonSlurper = new JsonSlurper()
  def object = jsonSlurper.parseText(result)
  return object
}

