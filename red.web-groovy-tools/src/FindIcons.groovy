import groovy.io.FileType

import org.apache.commons.io.FileUtils

def iconDirName = "/Users/hermesb/Development/workspaces/head/red.web-Application/src/eu/red_web/app/resource/"
File iconDir = new File(iconDirName)
def iconFiles = []
iconDir.eachFile(FileType.FILES){file->
    if(!file.name.startsWith(".")){
      iconFiles += file.name
    }
}

File srcDir = new File("/Users/hermesb/Development/workspaces/head/")
def srcFiles = []
srcDir.eachFileRecurse(FileType.FILES) {file->
    if(file.name.endsWith(".java") || file.name.endsWith(".js") || file.name.endsWith(".xml")){
      srcFiles += file
    }
}

srcFiles.each{file->
  def hits = find(file,iconFiles)
  hits.each {
    iconFiles.remove(it)
  } 
}

File unusedDir = new File("/Users/hermesb/Development/workspaces/head/red.web-Application/src/eu/red_web/app/resource/unused")
println "unused icons"
iconFiles.each {
  if(!it.equals("ResourceLoader.java")){
    File f = new File(iconDirName + it);
    FileUtils.moveFileToDirectory(f, unusedDir, false)
    println it
  }
}

def find(File file, iconFiles) {
    println "testing ${file.name}"
    def hits = []
    Scanner scanner = null
    try {
        scanner = new Scanner(new FileReader(file))
        while(scanner.hasNextLine()) {
            def line = scanner.nextLine()
            def isJava = file.name.endsWith(".java")
            iconFiles.each {
              def indexPos = line.indexOf(it); 
              def commentPos = isJava ? line.indexOf("//") : -1;
//              if(file.name.equals("AboutDialog.java") && it.equals("AboutImageOSX.png")){
//                println "about"
//              }
              
              if((commentPos < 0 && indexPos >= 0) || (commentPos >= 0 && indexPos >= 0 && indexPos < commentPos)){
                if(it.equals("AboutImageOSX.png")){
                  println "hit"
                }
                hits += it;
              }
            }
        }
    }
    catch(IOException e) {
        e.printStackTrace()
    }
    finally {
        try { scanner.close() ; } catch(Exception e) { /* ignore */ }
    }
    if(hits.size() > 0){
      println "checked ${file.name} hits: ${hits}"
    }
    return hits;
}
