package partpage

import java.text.SimpleDateFormat

import distantvoices3.client.BasicClient
import eu.red_web.server.client.ClientFactory


def server = "produktion4.red-web.org"
def user = "hermesb"
def passwd = "hermesb"
def eTag = "2013-04-25"

SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd",Locale.GERMANY)
SimpleDateFormat dateDayFormat = new SimpleDateFormat("EE",Locale.GERMANY)

Date date = Date.parse("yyyy-MM-dd", eTag)
day = dateDayFormat.format(date)

//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, 8100, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null){
  throw new Exception(keys[3])
}
cl.setPassword(keys[1])

def query = """
      <query>
          <select>partpage.id</select>
          <where>
              <field name=\"partpage_issues.date\" comparator=\"equal\" value=\"${eTag}\"/>
          </where>
      </query>
      """
println "Suche nach Teilseiten mit ETag: ${eTag}"

def ids = []
try {
  f.getTransactionDVProxy().open()
  def resultStr = f.getPartPageDVProxy().search(query, false)
  f.getTransactionDVProxy().commit()

  //println resultStr
  println "--------"
  def result = new XmlSlurper().parseText(resultStr);
  result.row.each{
    def id = it.partpage.field.@value.text()
    ids += id
  };
} catch (Exception e) {
  println "Fehler w�hrend der Suche der Teilseite ${name}: ${e.getMessage()}"
}
println "Partpage count: ${ids.size()}";
ids.each{id->
  println "Partpage: ${id}"
}


try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "Fehler w�hrend des Logouts: ${e.getMessage()}"
}




