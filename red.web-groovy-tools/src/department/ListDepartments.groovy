package department
import distantvoices3.client.BasicClient
import eu.red_web.app.core.Version
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.server.client.ClientFactory


def server = "redweb-t.it4media.de"
def port = 8100
//def server = "goofy.red-web.lan"
//def port = 8106
def user = "root"
def passwd = "itoito"
def enc = "UTF-8"

if(Version.APPLICATION_VERSION_STRING.startsWith("3.9")){
  TWSerializer.initTypeAliases()
}

BasicClient cl = new BasicClient(server, port, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])


try{
  f.getTransactionDVProxy().open()
  def clientIDs = f.getClientDVProxy().getClientIDs()
  clientIDs.each{ clientID ->
    //println "client: ${f.getClientDVProxy().getName(clientID)} id: ${clientID}"
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ objectID ->
      println "Publikation: ${f.getPublicationDVProxy().getName(objectID)} id: ${objectID}"
      def result = [:]
      def tree = [:]
      
      //println "\t\tdepartments"
      def depIDs = f.getDepartmentDVProxy().getIDs(objectID)
      depIDs.each{ depID->
        def depName = f.getDepartmentDVProxy().getName(depID,"de")
        def parentDep = f.getDepartmentDVProxy().getParentDepartment(depID)
        if(parentDep == null){
          parentDep = ""
        }
        //def item = "${depName.padRight(30)}id: ${depID.padLeft(20)}"
        def item = [depID,depName]
        if(tree[parentDep] == null){
          tree[parentDep] = []
        }
        tree[parentDep] += depID;
        result[depID] = item
        //println result[parentDep] 
      }
      tree[""]?.sort().each{id->
        def strID = result[id][0]
        def name = "- " + result[id][1]
        //strID = strID.padLeft(20);
        name = name.padRight(30);
        println "${name}${id}";
        tree[id]?.sort().each{subID->
          strID = result[subID][0]
          name = "  - " + result[subID][1]
          //strID = strID.padLeft(20);
          name = name.padRight(30);
          println "${name}${strID}";
        }
      }
      //result.sort().each{ println it; }
    }
  }
  f.getTransactionDVProxy().commit();
} catch (Exception e) {
  e.printStackTrace();
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}

