package article
import distantvoices3.client.BasicClient
import eu.red_web.app.core.Version
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.server.client.ClientFactory

def server = "127.0.0.1"
//def server = "goofy.red-web.lan"
//def server = "produktion.red-web.org"
def user = "hermesb"
def passwd = "hermesb"
def targetDir = "/Users/hermesb/temp/contentb0"
def enc = "UTF-8"

if(Version.APPLICATION_VERSION_STRING.startsWith("4.")){
  TWSerializer.initTypeAliases()
}

//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, 8100, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])

try{
  def File dir = new File("${targetDir}")
  if(!dir.exists()){
    dir.mkdir()
  }
  def date = Date.parse("yyyy-MM-dd","2012-10-12")
  def days = 1;
  def usedContentIDs = [:]
  for(int i=0; i < days; i++){
    def n=0;
    def dateStr = date.format("yyyy-MM-dd")
    def query = """
      <query>
          <select>article_content.id</select>
          <select>article_elementtopartpages_partpage_issues_edition.shortname</select>
          <where>
              <!--field name=\"article_elementtopartpages_partpage_issues.date\" comparator=\"equal\" value=\"${dateStr}\"/-->
              <field name=\"article_elementtopartpages_partpage_issues_edition.shortname\" comparator=\"like\" value=\"%\"/>
          </where>
      </query>
      """
    
    println "\n--------------------- export articles at ${dateStr} --------------------------------\n"
    def articleContentIDs = [] 
    try {
      f.getTransactionDVProxy().open()
      def resultStr = f.getArticleDVProxy().search(query, false)
      f.getTransactionDVProxy().commit()
      //println resultStr
      def result = new XmlSlurper().parseText(resultStr);
      articleContentIDs = result.row.article.articlecontent.field.findAll{it.@name=="article_content.id"}.@value.collect{it.text()}
    } catch (Exception e) {
      println "exception while getting content ids for ${dateStr}: ${e.getMessage()}"
      try {
        f.getTransactionDVProxy().rollback()
      } catch (Exception e2) {
        println "exception while rollbavk for getting content ids for ${dateStr}: ${e.getMessage()}"
      }
    }
    println "found: ${articleContentIDs.size}"
    articleContentIDs.eachWithIndex{it,index->
      if(usedContentIDs[it] != true){
        usedContentIDs[it] = true;
        def text = ""
        try{
          f.getTransactionDVProxy().open()
          f.getArticleDVProxy().testAndLockContent(it)
          text = f.getArticleDVProxy().getContentAsRHTML(it)
          println index
          f.getTransactionDVProxy().commit()
          f.getArticleDVProxy().unlockContent(it)
        } catch (Exception e) {
          println "exception while getting text for ${it}: ${e.getMessage()}"
          try {
            f.getTransactionDVProxy().rollback()
          } catch (Exception e2) {
            println "exception while rollback for getting text for ${it}: ${e2.getMessage()}"
          }
        }
      }
    }
    date = date.plus(1);
  }
}catch(Exception e){
  e.printStackTrace()
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}
