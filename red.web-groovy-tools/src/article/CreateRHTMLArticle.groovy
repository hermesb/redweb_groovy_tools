package article
import distantvoices3.client.BasicClient
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.server.client.ClientFactory

def server = "127.0.0.1"
def port = 8104
def user = "hermesb"
def passwd = "hermesb"

TWSerializer.initTypeAliases()


//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, port, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])

println "--------------------- create article --------------------------------"
def publicationID = "98d3e"
def pictogramID = "6d79793ba5f4d274"
def articleContentId = null
def articleId = null
try {
  if(publicationID != null){
    def keyword = "bibel-test-einzeln-02"
    def lock = true
    f.getTransactionDVProxy().open()
    if(articleId == null){
//      articleId = f.getArticleDVProxy().create(publicationID,keyword)
      articleId = f.getArticleDVProxy().createFromPictogram(publicationID, pictogramID, keyword)
      lock = false
    }
    articleContentId = f.getArticleDVProxy().getContentID(articleId)
    f.getTransactionDVProxy().commit()
    println "articleID: ${articleId} contentId: ${articleContentId}"
    if(lock){
      String[] lockResult = f.getArticleDVProxy().testAndLock(articleId)
      String[] contentLockResult = f.getArticleDVProxy().testAndLockContent(articleContentId)
      println "Lock result: ${lockResult} content lock: ${contentLockResult}"
    }
    f.getTransactionDVProxy().open()
    def text = new File("/Users/hermesb/bibel-test.xml").getText("UTF-8")
    f.getArticleDVProxy().setContentFromRHTML(articleContentId, text)
    f.getTransactionDVProxy().commit()
    if(lock){
      f.getArticleDVProxy().unlock(articleId)
      f.getArticleDVProxy().unlockContent(articleContentId)
    }
  }

} catch (Exception e) {
  try {
    f.getTransactionDVProxy().rollback()
  } catch (Exception e2) {
    println "rollback exception: ${e.getMessage()}"
  }
  println "exception occured: ${e.getMessage()}"
}
try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}

