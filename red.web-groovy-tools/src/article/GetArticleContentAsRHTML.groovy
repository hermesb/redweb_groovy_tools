package article
import distantvoices3.client.BasicClient
import eu.red_web.server.client.ClientFactory


def server = "sunrise.red-web.lan"
def user = "hermesb"
def passwd = "hermesb"
def enc = "UTF-8"

//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, 8100, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])

try{
  def articleID = "64b992bc602f5c38"
  def contentID = ""
  def text = ""
  try{
    f.getTransactionDVProxy().open()
//    f.getArticleDVProxy().testAndLockContent(contentID)
    
    def keyword = f.getArticleDVProxy().getKeyword(articleID)
    contentID = f.getArticleDVProxy().getContentID(articleID)
    text = f.getArticleDVProxy().getContentAsRHTML(contentID)
//    text = f.getArticleDVProxy().getVisibleTextAsEpaperML(articleID)
    println keyword
    println text
    f.getTransactionDVProxy().commit()
//    f.getArticleDVProxy().unlockContent(contentID)
    println "READY"
  } catch (Exception e) {
    println "exception while getting text for ${contentID}: ${e.getMessage()}"
    try {
      f.getTransactionDVProxy().rollback()
    } catch (Exception e2) {
      println "exception while rollback for getting text for ${contentID}: ${e2.getMessage()}"
    }
  }
}catch(Exception e){
  e.printStackTrace()
}
try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}
