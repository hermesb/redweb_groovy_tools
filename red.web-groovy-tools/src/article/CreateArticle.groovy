package article
import distantvoices3.client.BasicClient
import eu.red_web.app.core.Version
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.server.client.ClientFactory

def server = "neptun.red-web.lan"
def port = 8100
def user = "hermesb"
def passwd = "hermesb"

if(Version.APPLICATION_VERSION_STRING.startsWith("4.")){
  TWSerializer.initTypeAliases()
}


//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, port, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])

println "--------------------- create article --------------------------------"
def publicationID = "11"
def pictogramID = "d7f"
def articleContentId = null
try {
  if(publicationID != null){
    f.getTransactionDVProxy().open()
    def keyword = "article-settext-01"
    def create = false
    def articleId = create ? f.getArticleDVProxy().createFromPictogram(publicationID, pictogramID, keyword) : "4e00b68d6781c7df"
    articleContentId = f.getArticleDVProxy().getContentID(articleId)
    if(!create){
      f.getArticleDVProxy().testAndLock(articleId)
      f.getArticleDVProxy().testAndLockContent(articleContentId)
    }
    f.getArticleDVProxy().setTextForCategory(articleContentId, "Haupt�berschrift", "Eine Schlagzeile")
    f.getArticleDVProxy().setTextForCategory(articleContentId, "Unterzeile", "e�her�k45426565757 �kejh�kejth et�kh jet�hjke�hkj")
    f.getArticleDVProxy().setTextForCategory(articleContentId, "Textbereich","nichts mitttt mit \"Anf�hrungszeichen\"eehr drin, oder????")

    println "articleID: ${articleId} contentId: ${articleContentId}"
    f.getTransactionDVProxy().commit()
    f.getArticleDVProxy().unlock(articleId)
    f.getArticleDVProxy().unlockContent(articleContentId)
  }

} catch (Exception e) {
  try {
    f.getTransactionDVProxy().rollback()
  } catch (Exception e2) {
  }
  throw e
}
try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}
