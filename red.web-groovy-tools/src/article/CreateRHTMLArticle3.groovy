package article
import distantvoices3.client.BasicClient
import eu.red_web.server.client.ClientFactory

//def server = "produktion4.red-web.org"
def server = "127.0.0.1"
def port = 8104
def user = "hermesb"
def passwd = "hermesb"

//TWSerializer.initTypeAliases()

def text = new File("/Users/hermesb/test.xml").getText("UTF-8")


//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, port, user, null)
ClientFactory factory = new ClientFactory(cl)
String[] keys = factory.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])
println "--------------------- create article --------------------------------"
def publicationID = "98d3e"
def pictogramID = "6d79793ba5f4d274"
def articleContentId = null
def articleId = null
try {
  if(publicationID != null){
    def nr = Math.rint(Math.random() * 1000000)
    def keyword = "html-test-${nr}"
    def lock = true
    factory.getTransactionDVProxy().open()
    if(articleId == null){
      articleId = factory.getArticleDVProxy().create(publicationID, keyword)
//      articleId = factory.getArticleDVProxy().createFromPictogram(publicationID, pictogramID, keyword)
      lock = false
    }
    articleContentId = factory.getArticleDVProxy().getContentID(articleId)
    factory.getTransactionDVProxy().commit()
    if(lock){
      String[] lockResult = factory.getArticleDVProxy().testAndLock(articleId)
      String[] contentLockResult = factory.getArticleDVProxy().testAndLockContent(articleContentId)
      //        out "Lock result: ${lockResult} content lock: ${contentLockResult}"
    }
    factory.getTransactionDVProxy().open()
//    factory.getArticleDVProxy().setContentFromRHTML2(articleId,articleContentId, text)
    factory.getArticleDVProxy().setContentFromRHTML(articleContentId, text)
    println "articleID: ${articleId} contentId: ${articleContentId} Keyword: ${factory.getArticleDVProxy().getKeyword(articleId)}"
    factory.getTransactionDVProxy().commit()
    if(lock){
      factory.getArticleDVProxy().unlock(articleId)
      factory.getArticleDVProxy().unlockContent(articleContentId)
    }
  }
} catch (Exception e) {
  try {
    factory.getTransactionDVProxy().rollback()
  } catch (Exception e2) {
    println "rollback exception: ${e.getMessage()}"
  }
  println "exception occured: ${e.getMessage()}"
}
try{
  factory.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}



