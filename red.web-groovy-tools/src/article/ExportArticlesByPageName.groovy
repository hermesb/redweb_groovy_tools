package article
import distantvoices3.client.BasicClient
import eu.red_web.server.client.ClientFactory




def server = "rw4-services.red-web.org"
def user = "redweb"
def passwd = "redweb"
def targetDir = "/Users/hermesb/temp/contentrhtml"
def enc = "UTF-8"

//if(Version.APPLICATION_VERSION_STRING.startsWith("4.")){
//  TWSerializer.initTypeAliases()
//}


//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, 8100, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])
def ids = []

try{

  def query1 = """
      <query>
          <select>partpage.id</select>
          <where>
            <and>
              <field name=\"partpage_partpageplacements_page.keyword\" comparator=\"like\" value=\"w-CSS-%\"/>
            </and>
          </where>
      </query>
      """

  try {
    f.getTransactionDVProxy().open()
    def resultStr = f.getPartPageDVProxy().search(query1, false)
    f.getTransactionDVProxy().commit()
    def partePageCounter = 0
    def result = new XmlSlurper().parseText(resultStr)
    def partPageIDs = result.row.partpage.field.findAll{it.@name=="partpage.id"}.@value.collect{it.text()}

    partPageIDs.each{partPageID ->
      f.getTransactionDVProxy().open()
      def partPageKeyword = f.getPartPageDVProxy().getKeyword(partPageID)
      f.getTransactionDVProxy().commit()
  
      //println "checking partpage: ${partPageKeyword}"
      def query2 = """
        <query>
            <select>article.id</select>
            <where>
              <and>
                <field name=\"article_elementtopartpages_partpage.id\" comparator=\"equal\" value=\"${partPageID}\"/>
              </and>
            </where>
        </query>
        """
      f.getTransactionDVProxy().open()
      def resultStr2 = f.getArticleDVProxy().search(query2, false)
      f.getTransactionDVProxy().commit()
      def result2 = new XmlSlurper().parseText(resultStr2)
      //println "result2: ${resultStr2}"
      def articleIDs = result2.row.article.field.findAll{it.@name=="article.id"}.@value.collect{it.text()}
      //println articleContentIDs
      f.getTransactionDVProxy().open()
      def contains = "besitzt keine"
      def articles = ""
      def counter = 0
      articleIDs.each {
        def contentID = f.getArticleDVProxy().getContentID(it)
        text = f.getArticleDVProxy().getContentAsRHTML(contentID)
        if(text.indexOf("<style>") > 0){
          def pos1 = text.indexOf("<style")
          def pos2 = text.indexOf("</style")
          def styleText = text.substring(pos1,pos2)
          //println "Article: ${it} style: ${styleText}"
          if(!styleText.isEmpty()){
            def keyword = f.getArticleDVProxy().getKeyword(it)
            articles += "\n\tArtikel: ${keyword} id:${it}"
            contains = "enth�lt"
            counter++
          }
        }
        
      }
      f.getTransactionDVProxy().commit()
      if(counter > 0){
        partePageCounter++
        println "Teilseite: ${partPageKeyword} ${contains} Artikel mit festen Textattributen ${articles}"
      }
    }
    println "Anzahl der gefundenen Teilseiten mit Artikeln, die feste Textattribute besitzen: ${partePageCounter}"
    
  } catch (Exception e) {
    println "exception ${e.getMessage()}"
    try {
      f.getTransactionDVProxy().rollback()
    } catch (Exception e2) {
      println "exception while rollbacking: ${e.getMessage()}"
    }
  }

    
}catch(Exception e){
  e.printStackTrace()
}
try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}

