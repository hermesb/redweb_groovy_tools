

package article

import java.util.concurrent.Callable
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.ThreadLocalRandom

import org.apache.log4j.Logger

import distantvoices3.client.BasicClient
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.server.client.ClientFactory

def server = "127.0.0.1"
def port = 8104
def user = "hermesb"
def passwd = "hermesb"

TWSerializer.initTypeAliases()

def lines = new File("/Users/hermesb/Development/workspaces/head/red.web-Groovy-Scripts/src/article/bibel.txt").getText("UTF-8").split("\n")

Logger log = Logger.getLogger(getClass())

synchronized out(message) {
  //log.print(message)
  println(message)
}

def activeCount = 50
def articleCount = 2000

ExecutorService pool = Executors.newFixedThreadPool(activeCount)
defer = { c -> pool.submit(c as Callable) }

createArticle = {it -> 
  //MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
  println "--------------------- create article --------------------------------"
  def publicationID = "98d3e"
  def pictogramID = "6d79793ba5f4d274"
  def articleContentId = null
  def articleId = null
  ClientFactory factory = null
  String[] keys = null
  long duration1 = 0l
  long duration2 = 0l
  try {
    BasicClient cl = new BasicClient(server, port, user, null)
    factory = new ClientFactory(cl)
    keys = factory.getSessionDVProxy().login(user, null, passwd, null)
    if (keys[1]==null)
      throw new Exception(keys[3])
    
    cl.setPassword(keys[1])
    if(publicationID != null){
      def nr = Math.rint(Math.random() * 1000000)
      def keyword = "bibel-01-2016-08-01-${nr}"
      def lock = true
      factory.getTransactionDVProxy().open()
      if(articleId == null){
        articleId = factory.getArticleDVProxy().createFromPictogram(publicationID, pictogramID, keyword)
        lock = false
      }
      articleContentId = factory.getArticleDVProxy().getContentID(articleId)
      factory.getTransactionDVProxy().commit()
      if(lock){
        String[] lockResult = factory.getArticleDVProxy().testAndLock(articleId)
        String[] contentLockResult = factory.getArticleDVProxy().testAndLockContent(articleContentId)
      }
      
      def headline = lines[ThreadLocalRandom.current().nextInt(0, lines.length + 1)].replaceAll("\\s"," ").replaceAll("^[\\w]+[\\s]+[\\d]?[\\d]+:[\\d]?[\\d]+[\\s]+","").substring(0,20)
      def subheadline = lines[ThreadLocalRandom.current().nextInt(0, lines.length + 1)].replaceAll("\\s"," ").replaceAll("^[\\w]+[\\s]+[\\d]?[\\d]+:[\\d]?[\\d]+[\\s]+","").substring(0,30)
      
      def text = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><html><head data-format-version=\"\"><style/></head>"
      text += "<body><div class=\"headline\"><p><span>${headline}</span></p></div>"
      text += "<div class=\"subheadline\"><p><span>${subheadline}</span></p></div>"
      text += "<div class=\"basetext\">"
      
      def basetext = ""
      for(int i=0; i < ThreadLocalRandom.current().nextInt(5, 100); i++){
        def line = ""
        for(int j=0; j < ThreadLocalRandom.current().nextInt(4,15 ); j++){
          line += lines[ThreadLocalRandom.current().nextInt(0, lines.length + 1)].replaceAll("\\s"," ").replaceAll("^[\\w]+[\\s]+[\\d]?[\\d]+:[\\d]?[\\d]+[\\s]+","")
        }
        basetext += "<p><span>${line}</span></p>"
      } 
      def endtext = "</div></body></html>"
      
      text += basetext
      text += endtext
            
      long start = System.currentTimeMillis()
      factory.getTransactionDVProxy().open()
      log.info("create articleID: ${articleId} contentId: ${articleContentId} keyword: ${factory.getArticleDVProxy().getKeyword(articleId)}")
      factory.getArticleDVProxy().setContentFromRHTML2(articleId,articleContentId, text)
      long stop = System.currentTimeMillis()
      duration1 = stop - start
      start = System.currentTimeMillis()
      //factory.getArticleDVProxy().adjustHeight(articleId,0,0)
      stop = System.currentTimeMillis()
      duration2 = stop - start
      factory.getTransactionDVProxy().commit()
      if(lock){
        factory.getArticleDVProxy().unlock(articleId)
        factory.getArticleDVProxy().unlockContent(articleContentId)
      }
    }
  } catch (Exception e) {
    try {
      if(factory != null){
        factory.getTransactionDVProxy().rollback()
      }
    } catch (Exception e2) {
      log.error("rollback exception: ${e.getMessage()}")
    }
    log.error("exception occured: ${e.getMessage()}")
  } 
  try{
    if(factory != null && keys != null){
      factory.getSessionDVProxy().logout(user,keys[1])
    }
  }catch(Exception e){
    log.error("exception during logout: ${e.getMessage()}")
  }
  log.info("finished: ${Thread.currentThread().getName()} took: ${duration1} & ${duration2}")
}



for( j in 1..articleCount) {
  if(((java.util.concurrent.ThreadPoolExecutor)pool).getActiveCount() <= activeCount){ 
    defer{createArticle()}
  }else{
    log.warn("pool is full")
  }
  sleep 1000
  if(j % 10 == 0){
    log.info("main loop $j")
  }
}


pool.shutdown()
