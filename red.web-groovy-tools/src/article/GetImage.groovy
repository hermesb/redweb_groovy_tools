package article
import java.awt.image.BufferedImage

import javax.imageio.ImageIO
import javax.imageio.ImageReader

import distantvoices3.client.BasicClient
import eu.red_web.server.client.ClientFactory


def server = "redweb.me-intern.de"
def user = "hermesb"
def passwd = "hermesb"
def enc = "UTF-8"

//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, 8100, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])

try{
  def contentID = "61f9fabbdaf809c"
  try{
    f.getTransactionDVProxy().open()
    f.getFotoDVProxy().testAndLock(contentID)

    def b = f.getFotoDVProxy().getLowres(contentID,0);
    f.getTransactionDVProxy().commit()
    f.getFotoDVProxy().unlock(contentID)
    File outputfile = new File("/Users/hermesb/image.jpg");
    FileOutputStream fos = null;
    try {
      fos = new FileOutputStream(outputfile);
      // Writes bytes from the specified byte array to this file output stream
      fos.write(b);

    }
    catch (FileNotFoundException e) {
      System.out.println("File not found" + e);
    }
    catch (IOException ioe) {
      System.out.println("Exception while writing file " + ioe);
    }
    finally {
      // close the streams using close method
      try {
        if (fos != null) {
          fos.close();
        }
      }
      catch (IOException ioe) {
        System.out.println("Error while closing stream: " + ioe);
      }

    }


    println "READY"
    Iterator<ImageReader> readers = ImageIO.getImageReadersByFormatName("JPEG");
    while (readers.hasNext()) {
        System.out.println("reader: " + readers.next());
    }
    File file = new File("/Users/hermesb/image.jpg");
    BufferedImage bufferedImage = ImageIO.read( file );

    File outputfile2 = new File("/Users/hermesb/image2.jpg");
    ImageIO.write(bufferedImage,"jpg",outputfile2);

  } catch (Exception e) {
    println "exception while getting text for ${contentID}: ${e.getMessage()}"
    try {
      f.getTransactionDVProxy().rollback()
    } catch (Exception e2) {
      println "exception while rollback for getting text for ${contentID}: ${e2.getMessage()}"
    }
  }
}catch(Exception e){
  e.printStackTrace()
}
try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}
