package article
import java.awt.image.BufferedImage

import javax.imageio.ImageIO

import distantvoices3.client.BasicClient
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.server.client.ClientFactory

def server = "panther.red-web.com"
def port = 8100
def user = "hermesb"
def passwd = "hermesb"

TWSerializer.initTypeAliases()


//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, port, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])

println "This is a Test!"


println "--------------------- create article --------------------------------"
def publicationID = "57b5bd"
def pictogramID = "46db4a7738a8f422"
def articleContentId = null
def articleId = null;
try {
  if(publicationID != null){
    def keyword = "xyz-test-01"
    def lock = true
    f.getTransactionDVProxy().open()
    if(articleId == null){
      articleId = f.getArticleDVProxy().createFromPictogram(publicationID, pictogramID, keyword)
      lock = false
    }
    articleContentId = f.getArticleDVProxy().getContentID(articleId)
    println "articleID: ${articleId} contentId: ${articleContentId}"
    if(lock){
      String[] lockResult = f.getArticleDVProxy().testAndLock(articleId)
      String[] contentLockResult = f.getArticleDVProxy().testAndLockContent(articleContentId)
      println "Lock result: ${lockResult} content lock: ${contentLockResult}"
    }
    def size = f.getArticleDVProxy().getSize(articleId)
    def w = size[0]
    def h = size[1]
    //f.getArticleDVProxy().setSize(articleId,w,h*2)
    f.getArticleDVProxy().setSizeAndRelayout(articleId,w,h*2)
    def text = new File("/Users/hermesb/test.xml").getText("UTF-8");
    f.getArticleDVProxy().setContentFromRHTML(articleContentId, text)
    if(lock){
      f.getArticleDVProxy().unlock(articleId)
      f.getArticleDVProxy().unlockContent(articleContentId)
    }
//    f.getTransactionDVProxy().commit()
//    
//    f.getTransactionDVProxy().open()
//    String[] lockResult = f.getArticleDVProxy().testAndLock(articleId)
//    println "Lock result: ${lockResult}"
    //f.getArticleDVProxy().setSizeAndRelayoutarticleId,w,h*2)
    def buf = f.getArticleDVProxy().createPreviewFixedSize(articleId,600,600)
    BufferedImage img = ImageIO.read(new ByteArrayInputStream(buf));
    File outputfile = new File("/Users/hermesb/image.jpg");
    ImageIO.write(img,"jpg",outputfile);
//    f.getTransactionDVProxy().commit()
//    f.getArticleDVProxy().unlock(articleId)
    
  }

} catch (Exception e) {
  try {
    f.getTransactionDVProxy().rollback()
  } catch (Exception e2) {
    println "rollback exception: ${e.getMessage()}"
  }
  println "exception occured: ${e.getMessage()}"
}
try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}

