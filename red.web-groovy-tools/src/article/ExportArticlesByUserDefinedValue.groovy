package article
import distantvoices3.client.BasicClient
import eu.red_web.app.core.Version
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.server.client.ClientFactory
import groovy.json.JsonSlurper


def server = "redweb4.local"
def user = "hermesb"
def passwd = "hermesb"
def enc = "UTF-8"

if(Version.APPLICATION_VERSION_STRING.startsWith("4.")){
  TWSerializer.initTypeAliases()
}


//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, 8104, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])
def ids = []

try{
  def queryJson1 = """
{
  "select": ["id", "meta.sourcesystem.id", "meta.sourcechannel.id", "meta.documentcreator", "meta.documentsource", "redweb.createopid", "meta.createdby.name", "meta.created", "meta.modifiedby.name", "meta.modified", "meta.name", "meta.variantcomment", "meta.publications.publication.id", "meta.keywords.keyword.value"],
  "where": {
    "and": [{
      "field": {
        "name": "meta.sourcesystem.id",
        "comparator": "=",
        "value": 8
      }
    }, {
      "and": [{
        "field": {
          "name": "meta.modified",
          "comparator": ">=",
          "value": "2016-03-31T00:00:38+0200"
        }
      }, {
        "field": {
          "name": "meta.modified",
          "comparator": "<=",
          "value": "2016-04-01T12:00:38+0200"
        }
      }, ]
    }, ]
  },
  "orderby": [{
    "field": "meta.name",
    "descending": false
  }, ],
  "limit": 250,
}
  """

  println "\n--------------------- export articles  --------------------------------\n"
  try {
    f.getTransactionDVProxy().open()
    def resultStr = f.getFotoDVProxy().contentAdvancedJSONSearch(queryJson1, false)
    def jsonSlurper = new JsonSlurper()
    if(resultStr != null && !resultStr.isEmpty()){
      json = jsonSlurper.parseText(resultStr)
      ids = json.objects.collect{it.redweb.id}
      println "ids containing user defined values: ${ids}"
    }
    f.getTransactionDVProxy().commit()
  } catch (Exception e) {
    println "exception while getting content ids: ${e.getMessage()}"
    try {
      f.getTransactionDVProxy().rollback()
    } catch (Exception e2) {
      println "exception while rollbavk for getting content ids: ${e.getMessage()}"
    }
  }
  
/*  
  ids.each{
    def contentID = eu.red_web.commons.utils.ID.toHexForm(it)
    try{
      f.getTransactionDVProxy().open()
      f.getArticleDVProxy().testAndLockContent(contentID)
      println "setting user defined value ARTICLEPRIO for ${contentID}"
      f.getArticleDVProxy().setContentUserdefinedValue(contentID,"ARTICLEPRIO","ohne Prio")
      f.getTransactionDVProxy().commit()
      f.getArticleDVProxy().unlockContent(contentID)
    } catch (Exception e) {
      println "exception while getting text for ${it}: ${e.getMessage()}"
      try {
        f.getTransactionDVProxy().rollback()
      } catch (Exception e2) {
        println "exception while rollback for getting text for ${it}: ${e2.getMessage()}"
      }
    }
  }
*/
}catch(Exception e){
  e.printStackTrace()
}
try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}
