package stylesheet
import distantvoices3.client.BasicClient
import distantvoices3.client.BasicClient
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.layout.pictogram.controller.PictogramController
import eu.red_web.library.compression.CompressionUtil
import eu.red_web.server.client.ClientFactory




def server = "rw4.it4media.de"
def port = 8100
def user = "hermesb"
def passwd = "hermesb"
def userHome = System.getProperty("user.home")
def enc = "UTF-8"
def targetDir = "${userHome}/temp/css/${server}"

TWSerializer.initTypeAliases()

//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, port, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])

println "--------------------- set first stylesheet active --------------------------------"
try{
  f.getTransactionDVProxy().open()
  def clientIDs = f.getClientDVProxy().getClientIDs()
  clientIDs.each{ clientID ->
    if(clientID != null){
      def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
      objectIDs.each{publicationID ->
        if(publicationID != null){
          allPictoIDs = f.getPictogramDVProxy().getIDs(publicationID)
          allPictoIDs.each{ pictoID->
            def pictoBytes = f.getPictogramDVProxy().download(pictoID)
            def pictoXML = new String(CompressionUtil.inflate(pictoBytes),enc)
            def name1 = f.getPictogramDVProxy().getName(pictoID,0)
            def name2 = f.getPictogramDVProxy().getName(pictoID,1)
            def name3 = f.getPictogramDVProxy().getName(pictoID,2)
            if(name3 == null){
              name3 = name2
            }
            def pictoName = "${name1}/${name2} ${name3}"
            println "Pikto ${pictoName}"
            
            PictogramController newPc = this.getClass().getClassLoader().loadClass("eu.red_web.layout.pictogram.controller.PictogramController").newInstance()
            newPc.fromDocument(pictoXML, true)
            def tempSelStyleIds = newPc.getStyleIDs()
            //println "Stylesheets: ${tempSelStyleIds}"
            def selStyleIds = []
            tempSelStyleIds.each{styleID->
              def styleIDHex = eu.red_web.commons.utils.ID.toHexForm(styleID)
              def styleName = f.getStyleDVProxy().getName(styleIDHex,null)
              def styleGroup = f.getStyleDVProxy().getStyleGroup(styleIDHex)
              def stylesheetName = "${styleGroup}/${styleName}"
              //println "\t${stylesheetName}"
              if(!stylesheetName.equals(pictoName)){
                selStyleIds += styleID
              }else{
                println "removing stylesheet(${styleID}): ${stylesheetName}"
                newPc.getStylesheetsController().removeStylesheetId(styleID)
              }
            }
            
            if(selStyleIds.size() > 0){
              println "fixed stylesheet ids: ${selStyleIds}"
              newPc.getStylesheetsController().setActivated(selStyleIds[0])
              //println "Piktogram ${pictoID} active style: ${newPc.getStylesheetsController().getActivatedStylesheetId()}"
              if(selStyleIds.size() > 1){
                println "\t\t\t\t\t\tWarning!!!!!!!!!! multiple stylesheets still assigned for ${pictoName}"
              }
              newPc.setUseStylesheets(true)
              byte[] newPCAsBytes = newPc.toDocument()
              String pictoXMLNew = new String(newPCAsBytes,enc)
              //println "uploading pictogram ${pictoID}\n${pictoXMLNew}"
              //f.getPictogramDVProxy().upload(CompressionUtil.deflate(newPCAsBytes),clientID,publicationID)
            }
          }
        }
      }
    }
  }
  f.getTransactionDVProxy().commit()
} catch (Exception e) {
  try {
    f.getTransactionDVProxy().rollback()
  } catch (Exception e2) {
  }
  throw e
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}

