package stylesheet

import org.apache.commons.io.FileUtils

import distantvoices3.client.BasicClient
import eu.red_web.app.core.Version
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.library.compression.CompressionUtil
import eu.red_web.server.client.ClientFactory

def server = "panther.red-web.com"
def port = 8100
def user = "hermesb"
def passwd = "hermesb"
def enc = "UTF-8"
def targetDir = "/Users/hermesb/temp/styles/"

if(Version.APPLICATION_VERSION_STRING.startsWith("4.")){
  TWSerializer.initTypeAliases()
}

BasicClient cl = new BasicClient(server, port, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])


try{
  f.getTransactionDVProxy().open()
  def clientIDs = f.getClientDVProxy().getClientIDs()
  clientIDs.each{ clientID ->
    def clientName = f.getClientDVProxy().getName(clientID)
    println "client id: ${clientID} name: ${clientName}"
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ objectID ->
      def objectName = f.getPublicationDVProxy().getName(objectID)
      println "\tobject id: ${objectID} name: ${objectName}"
      def ids = f.getStyleDVProxy().getAllIDs(clientID,objectID)
      ids.each{styleID->
        byte[] styleContentAsBytes = f.getStyleDVProxy().download(styleID)
        def styleContent = new String(CompressionUtil.inflate(styleContentAsBytes),enc)
        def name = f.getStyleDVProxy().getName(styleID,null)
        def group = f.getStyleDVProxy().getStyleGroup(styleID)

        group = group.replaceAll("\\\\" ,"#backslash#")
        group = group.replaceAll("\\/" ,"#slash#")
        group = group.replaceAll("<","#lower#")
        group = group.replaceAll(">","#larger#")

        name = name.replaceAll("\\\\","#backslash#")
        name = name.replaceAll("\\/","#slash#")
        name = name.replaceAll("<","#lower#")
        name = name.replaceAll(">","#larger#")


        def styleDir = "${targetDir}${File.separator}${clientName}${File.separator}${objectName}${File.separator}${group}"
        def styleFile = "${styleDir}${File.separator}${name}.css"
        println "backup style to ${styleFile}"
        FileUtils.forceMkdir(new File(styleDir))
        def file = new File(styleFile)
        file.write(styleContent, enc)
      }
    }
  }
  f.getTransactionDVProxy().commit()
} catch (Exception e) {
  try {
    f.getTransactionDVProxy().rollback()
  } catch (Exception e2) {
  }
  throw e
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}


