



package stylesheet
import java.util.regex.Matcher
import java.util.regex.Pattern

import distantvoices3.client.BasicClient
import eu.red_web.library.compression.CompressionUtil
import eu.red_web.server.client.ClientFactory



def server = "paula.red-web.lan"
def user = "hermesb"
def passwd = "hermesb"
def userHome = System.getProperty("user.home")
def enc = "UTF-8"

//TWSerializer.initTypeAliases()

//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, 8100, user, null);
ClientFactory f = new ClientFactory(cl);
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null);
if (keys[1]==null)
  throw new Exception(keys[3]);

cl.setPassword(keys[1]);


println "--------------------- search all stylesheets --------------------------------"
try{
  f.getTransactionDVProxy().open()
  def clientIDs = f.getClientDVProxy().getClientIDs()
  clientIDs.each{ clientID ->
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ publicationID ->
      def ids = f.getStyleDVProxy().getAllIDs(clientID,publicationID)
      ids.each{id->
        byte[] styleContentAsBytes = f.getStyleDVProxy().download(id)
        def styleContent = new String(CompressionUtil.inflate(styleContentAsBytes),enc)
        def styleName = f.getStyleDVProxy().getName(id,null)
        def styleGroup = f.getStyleDVProxy().getStyleGroup(id)
        Pattern p = Pattern.compile("@import[\\s]+\"(263ad616f1cdcd56)\"")
        Matcher m = p.matcher(styleContent)
        if(m.find()){
          styleContent = m.replaceAll(/@import "184dca95a1450884"/);
          println "Stylesheets ${styleGroup}/${styleName} enth�lt fehlerhafte"
          println "korrigiert: ${styleContent}"
          styleContentAsBytes = styleContent.getBytes(enc)
          f.getStyleDVProxy().upload(CompressionUtil.deflate(styleContentAsBytes),id,clientID,publicationID,styleName);
        }
      }
    }
  }
} catch (Exception e) {
  e.printStackTrace();
  try {
    f.getTransactionDVProxy().rollback();
  } catch (Exception e2) {
    e2.printStackTrace();
  }
  throw e;
} finally{
  f.getTransactionDVProxy().commit();
}
try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}

