package stylesheet
import javax.swing.text.SimpleAttributeSet

import distantvoices3.client.BasicClient
import eu.red_web.app.core.Version
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.editor.css.StyleMappings
import eu.red_web.editor.text.RWTextAttributes
import eu.red_web.library.algorithms.MD5
import eu.red_web.library.compression.CompressionUtil
import eu.red_web.registry.embeddedserverimpl.ESInternalRegistry
import eu.red_web.server.Properties
import eu.red_web.server.client.ClientFactory
import eu.red_web.server.embedded.EmbeddedServer
import eu.red_web.server.embedded.MasterControl
import eu.red_web.server.embedded.sync.ConfigurationDataSynchronisation
import eu.red_web.server.embedded.sync.SynchronisationException
import eu.red_web.server.services.wrapper.EmbeddedIOLayer

def slash = System.getProperty("file.separator")

def server = "127.0.0.1"
def port = 8104
def user = "hermesb"
def passwd = "hermesb"
def userHome = System.getProperty("user.home")
def enc = "UTF-8"
def targetDir = "${userHome}/temp/css/${server}"
def File targetDirFile = new File(targetDir)
if(!targetDirFile.exists()){
  targetDirFile.mkdir()
}

if(Version.APPLICATION_VERSION_STRING.startsWith("4.")){
  TWSerializer.initTypeAliases()
}

try {
  System.setProperty(Properties.HBM2DDL_MODE_OVERRIDE, "none")
  def urlStr = "http://${server}:8080/server/dv?dvPort=${port}"
  URL url = new URL(urlStr)
  EmbeddedServer.startup(url)
  MasterControl.init()
  String[] xx = MasterControl.getInstance().connect(user,passwd,"null","null")
  sessionID = xx[1]
  EmbeddedIOLayer.startup(user)
  boolean synchronize = true
  if (synchronize) {
    try {
      ConfigurationDataSynchronisation cfg = new ConfigurationDataSynchronisation()
      cfg.setSyncItems(null)
      cfg.synchronize(MasterControl.getInstance().getForegroundFactory())
    } catch (SynchronisationException e) {
      e.printStackTrace()
    }
  }
  new ESInternalRegistry().startup()
} catch (Exception  e) {
  e.printStackTrace()
}

BasicClient cl = MasterControl.getInstance().getForegroundDVClient()
ClientFactory f = new ClientFactory(cl)

/*
//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, port, user, null);
ClientFactory f = new ClientFactory(cl);
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null);
if (keys[1]==null)
  throw new Exception(keys[3]);

cl.setPassword(keys[1]);
*/

try{
  f.getTransactionDVProxy().open()
  def clientIDs = f.getClientDVProxy().getClientIDs()
  clientIDs.each{ clientID ->
    def clientName = f.getClientDVProxy().getName(clientID)//.replaceAll("[ /]", "_")
    def clientDirStr = "${targetDir}/${clientName}"
    println "client id: ${clientID} name: ${clientName} clientDir: ${clientDirStr}"
    def File clientDir = new File(clientDirStr)
    if(!clientDir.exists()){
      clientDir.mkdir()
    }
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ objectID ->
      def objectName = f.getPublicationDVProxy().getName(objectID)//.replaceAll("[ /]", "_")
      //if(objectName.compareTo("Tageszeitung") == 0){
        println "object id: ${objectID} name: ${objectName}"
        def File objectDir = new File("${targetDir}/${clientName}/${objectName}")
        if(!objectDir.exists()){
          objectDir.mkdir()
        }
        def labelDir = new File("${targetDir}/${clientName}/${objectName}/Auszeichnungen/")
        if(!labelDir.exists()){
          def created = labelDir.mkdir()
          println "labelDir is ${created}"
        }
        def sysDir = new File("${targetDir}/${clientName}/${objectName}/SystemCSS/")
        if(!sysDir.exists()){
          sysDir.mkdir()
        }
        def cssFile = new File("${targetDir}/${clientName}/${objectName}/Auszeichnungen/auszeichnungen.css")
        def labelLongID = MD5.createUniqueKey()
        def labelStyleID = labelLongID
        cssFile.write("/*ifdef\n${labelStyleID}\nenddef*/\n", enc)
  
        cssFile = new File("${targetDir}/${clientName}/${objectName}/SystemCSS/default.css")
        def systemStyleID = MD5.createUniqueKey()
        def defaultCSSValues = """
*{
   font: Sabon LT Bold;
   font-size: 9.5pt;
   -rw-font-scale-x: 94.0%;
   -rw-font-scale-y: 100.0%;
   letter-spacing: 0.2%;
   baselineshift: 0.0pt;
   -rw-kerning: false;
   -rw-ligatures: false;
   color: black;
   background: none;
   opacity: 100.0%;
   -rw-background-opacity: 100.0%;
   -rw-linked-lines: none;
   -rw-lines: none;
   -rw-forbid-hyphenation: true;
   text-align: left;
   -rw-text-indent-start: 0.0pt;
   text-indent: 0.0pt;
   margin-left: 0.0pt;
   margin-right: 0.0pt;
   margin-top: 0.0pt;
   line-height: 10.0pt;
   -rw-leading: 0.0pt;
   vertical-align: 0.0pt;
   word-spacing: 15.0 25.0 40.0;
   -rw-tab-set: none;
   -rw-tab-width: 5.0mm;
   -rw-register: false;
   -rw-initial: none;
   -rw-column-break-after: false;
   -rw-avoid-fitin: false;
   -rw-margin-top-ignore-start: false;
   display: inline;
   -rw-outline: none;
   text-shadow: none;
}
        """
        cssFile.write("/*ifdef\n${systemStyleID}\nenddef*/\n${defaultCSSValues}\n", enc)
  
  
        def pictoIDs = f.getPictogramDVProxy().getIDs(objectID)
        pictoIDs.each{ pictoID->
          def pictoBytes = f.getPictogramDVProxy().download(pictoID)
          def pictoXML = new String(CompressionUtil.inflate(pictoBytes),enc)
          def name1 = f.getPictogramDVProxy().getName(pictoID,0)?.replaceAll("[/]", "_")
          def name2 = f.getPictogramDVProxy().getName(pictoID,1)?.replaceAll("[/]", "_")
          def name3 = f.getPictogramDVProxy().getName(pictoID,2)?.replaceAll("[/]", "_")
          def pictoDirName = "${targetDir}/${clientName}/${objectName}/${name1}/"
          println "pictogramm id: ${pictoID} name: ${pictoDirName}"
          def File pictoDir = new File(pictoDirName)
          if(!pictoDir.exists()){
            pictoDir.mkdir()
          }
          eu.red_web.layout.pictogram.controller.PictogramController pictoController = this.getClass().getClassLoader().loadClass("eu.red_web.layout.pictogram.controller.PictogramController").newInstance()
          try{
            pictoController.fromDocument(pictoXML, true)
            def styleID = MD5.createUniqueKey()
            def cssContent = "/*ifdef\n${styleID}\n${labelStyleID}\n${pictoID}\nenddef*/\n"
            pictoController.getAreaController().getPictoareanames().each{name->
              RWTextAttributes t = pictoController.getAreaController().getPictoareaControllerByName(name).getTextAttributes()
              if(t == null){
                println "Textattributes are null for ${name}"
              }else{
                def charAttr = t.getCharAttributeSet()
                def paraAttr = t.getParagraphAttributeSet()
                SimpleAttributeSet allAttr = new SimpleAttributeSet()
                allAttr.addAttributes(charAttr)
                allAttr.addAttributes(paraAttr)
                def cssDef = StyleMappings.getInstance().attributeToCss(objectID, allAttr,true).replaceAll(";", ";\n\t")
                cssContent += "\n.${name}{\n\t${cssDef}\n}\n\n"
                //          def cssCharDef = StyleMappings.getInstance().attributeToCss(objectID, charAttr,true).replaceAll(";", ";\n\t")
                //          def cssParaDef = StyleMappings.getInstance().attributeToCss(objectID, paraAttr,true).replaceAll(";", ";\n\t")
                //          cssContent += "\n.${name}{\n\t${cssCharDef}\n\t${cssParaDef}\n}\n\n";
              }
            }
            //        def cssDef = "\n.${name}{\n\t${cssCharDef}\n\t${cssParaDef}\n}\n\n";
            cssFileName = (name2 != null && !name2.isEmpty() && name3 != null && !name3.isEmpty()) ? "${pictoDirName}${name2}_${name3}.css" : "${pictoDirName}${name1}.css"
            println "cssFileName: ${cssFileName}"
            cssFile = new File(cssFileName)
            cssFile.write(cssContent, enc)
          }catch(Exception ex){
            println "Exception while converting ${name1}/${name2}/${name3}: ${ex.message}"
          }
        }
      //}
    }
  }
  f.getTransactionDVProxy().commit()
} catch (Exception e) {
  e.printStackTrace()
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}




