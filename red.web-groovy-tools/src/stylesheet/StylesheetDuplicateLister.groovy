package stylesheet
import distantvoices3.client.BasicClient
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.layout.pictogram.controller.PictogramController
import eu.red_web.library.compression.CompressionUtil
import eu.red_web.server.client.ClientFactory



def server = "rw4-users-1.red-web.org"
def user = "hermesb"
def passwd = ""
def userHome = System.getProperty("user.home")
def enc = "UTF-8"

TWSerializer.initTypeAliases()

//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, 8100, user, null);
ClientFactory f = new ClientFactory(cl);
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null);
if (keys[1]==null)
  throw new Exception(keys[3]);

cl.setPassword(keys[1]);


println "--------------------- search all stylesheets --------------------------------"
def counterByPublication = [:]
def assignedStyle = [:]

try{
  f.getTransactionDVProxy().open()
  def clientIDs = f.getClientDVProxy().getClientIDs()
  clientIDs.each{ clientID ->
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ publicationID ->
      if(publicationID == "57b5bc"){
        println " object name: ${f.getPublicationDVProxy().getName(publicationID).padRight(35)} id: ${publicationID}"

        def usedStylesheets = [:]
        def allPictoIDs = f.getPictogramDVProxy().getIDs(publicationID)
        allPictoIDs.each{ pictoID->
          def pictoBytes = f.getPictogramDVProxy().download(pictoID)
          def pictoXML = new String(CompressionUtil.inflate(pictoBytes),enc);
          PictogramController newPc = this.getClass().getClassLoader().loadClass("eu.red_web.layout.pictogram.controller.PictogramController").newInstance()
          newPc.fromDocument(pictoXML, true)
          def styleIDList = new ArrayList<Long>(newPc.getSelectedStylesheetIdList())
          styleIDList.each{styleID->
            def styleIDHex = eu.red_web.commons.utils.ID.toHexForm(styleID)
            usedStylesheets[styleIDHex] = true
          }
          //          println "Piktogram ${pictoID} used stylesheet: ${usedStylesheets.size()}"
        }
        println "used stylesheets: ${usedStylesheets}";
        def counter = [:]
        def ids = f.getStyleDVProxy().getAllIDs(clientID,publicationID)
        def idsCount = ids.size()
        ids.eachWithIndex{id,index->
          if(usedStylesheets[id] == null || usedStylesheets[id] == false){
            def styleName = f.getStyleDVProxy().getName(id,null)
            def styleGroup = f.getStyleDVProxy().getStyleGroup(id)
            def name = styleGroup + "/" + styleName
            if(counter[name] == null){
              counter[name] = []
            }
            counter[name].add(id)
            //            println "${index}/${idsCount}"
          }
          if(usedStylesheets[id] != null && usedStylesheets[id] == true){
            def styleName = f.getStyleDVProxy().getName(id,null)
            def styleGroup = f.getStyleDVProxy().getStyleGroup(id)
            def name = styleGroup + "/" + styleName
            assignedStyle[name] = id
          }
        }
        counterByPublication[publicationID] = counter;
      }
    }
  }
} catch (Exception e) {
  e.printStackTrace();
  try {
    f.getTransactionDVProxy().rollback();
  } catch (Exception e2) {
    e2.printStackTrace();
  }
  throw e;
} finally{
  f.getTransactionDVProxy().commit();
}

counterByPublication.each{id,counter->
  counter.each {name,multipleValues->
    if(multipleValues != null && multipleValues.size() > 1){
      println "${name}: ${assignedStyle[name]} - ${multipleValues}"
      if(assignedStyle[name] == null){
        def keep = multipleValues.pop()
        //              println "keep: ${keep}"
        //              println "rest: ${multipleValues}"
      }
      if(false){
        multipleValues.each{duplicateID->
          try{
            f.getTransactionDVProxy().open()
            def styleName = f.getStyleDVProxy().getName(duplicateID,null)
            def styleGroup = f.getStyleDVProxy().getStyleGroup(duplicateID)
            def tempName = styleGroup + "/" + styleName
            println "deleting ${duplicateID}: ${tempName}"
            //f.getStyleDVProxy().markForDeletion(duplicateID)
          } catch (Exception e) {
            e.printStackTrace();
            try {
              f.getTransactionDVProxy().rollback();
            } catch (Exception e2) {
              e2.printStackTrace();
            }
            throw e;
          } finally{
            f.getTransactionDVProxy().commit();
          }
        }
      }
    }
  }
}


try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}

