package stylesheet
import distantvoices3.client.BasicClient
import eu.red_web.app.core.Version
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.library.compression.CompressionUtil
import eu.red_web.server.client.ClientFactory



def server = "192.168.119.129"
def user = "hermesb"
def passwd = "hermesb"
def userHome = System.getProperty("user.home")
def enc = "UTF-8"
def cssSrcDir = /d:\temp\cssArticleLayouts/
def clientID = "4d234d618df284b776b5dab0ad3ef6701936b60f693d4cdf3076fd6b684f5b73"
def publicationID = "745dbed41901eb62a91b26a9c9e8374360fed3ab5441ff5bb9fc73fedd0cdbfc"

if(Version.APPLICATION_VERSION_STRING.startsWith("4.")){
  TWSerializer.initTypeAliases()
}

//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, 8100, user, null);
ClientFactory f = new ClientFactory(cl);
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null);
if (keys[1]==null)
  throw new Exception(keys[3]);

cl.setPassword(keys[1]);



println "--------------------- upload stylesheets --------------------------------"

int i = 1;
new File(cssSrcDir).eachFileMatch(~/.*\.css/){ file->
  String styleContent = file.getText(enc);
  def fileNameWithoutExt = file.name.replaceAll("\\.css", "")
  //println "${fileNameWithoutExt}:\n${styleContent}\n............................................................................................"
  def styleID = fileNameWithoutExt
  def startString = "/*start picto ids"
  def endString = "end picto ids*/"
  def pictoIDBlockStart = styleContent.indexOf(startString)
  def pictoIDBlockEnd = styleContent.indexOf(endString)
  def pictoIDs = ""
  if(pictoIDBlockStart >= 0 && pictoIDBlockEnd >=0){
    def tempStart = pictoIDBlockStart + startString.length() + 1 < styleContent.length() ? pictoIDBlockStart + startString.length() + 1 : pictoIDBlockStart + startString.length()
    def tempEnd = pictoIDBlockEnd + endString.length() + 1 < styleContent.length() ? pictoIDBlockEnd + endString.length() + 1 : pictoIDBlockEnd + endString.length()
    pictoIDs = styleContent.substring(tempStart, pictoIDBlockEnd)
    styleContent = styleContent.substring(tempEnd)
    //println "style: ${styleID}\n${styleContent} associated with:\n ${pictoIDs}"
  }
  println "updating style: ${styleID}"
  byte[] styleContentAsBytes = styleContent.getBytes(enc)
  try{
    f.getTransactionDVProxy().open()
    f.getStyleDVProxy().upload(CompressionUtil.deflate(styleContentAsBytes),styleID,clientID,publicationID,styleID);
    f.getTransactionDVProxy().commit();
  } catch (Exception e) {
    try {
      f.getTransactionDVProxy().rollback();
    } catch (Exception e2) {
    }
    throw e;
  }
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}
