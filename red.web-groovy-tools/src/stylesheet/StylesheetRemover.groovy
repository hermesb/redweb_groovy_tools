package stylesheet
import distantvoices3.client.BasicClient
import eu.red_web.app.core.Version
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.server.client.ClientFactory



def server = "neptun.red-web.lan"
def user = "hermesb"
def passwd = "hermesb"
def userHome = System.getProperty("user.home")
def enc = "UTF-8"
//def clientID = "4d234d618df284b776b5dab0ad3ef6701936b60f693d4cdf3076fd6b684f5b73"
//def publicationID = "745dbed41901eb62a91b26a9c9e8374360fed3ab5441ff5bb9fc73fedd0cdbfc"

if(Version.APPLICATION_VERSION_STRING.startsWith("3.9")){
  TWSerializer.initTypeAliases()
}

//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, 8100, user, null);
ClientFactory f = new ClientFactory(cl);
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null);
if (keys[1]==null)
  throw new Exception(keys[3]);

cl.setPassword(keys[1]);


/*
println "--------------------- unlink stylesheets from pictos --------------------------------"
try {
  f.getTransactionDVProxy().open();
  def clientIDs = f.getClientDVProxy().getClientIDs()
  clientIDs.each{ clientID ->
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ publicationID ->
      def allPictoIDs = f.getPictogramDVProxy().getIDs(publicationID)
      allPictoIDs.each{ pictoID->
        def pictoBytes = f.getPictogramDVProxy().download(pictoID)
        def pictoXML = new String(CompressionUtil.inflate(pictoBytes),enc);
        PictogramController newPc = this.getClass().getClassLoader().loadClass("eu.red_web.layout.pictogram.controller.PictogramController").newInstance()
        newPc.fromDocument(pictoXML, true)
        def oldIDList = new ArrayList(newPc.getSelectedStylesheetIdList())
        oldIDList.each{oldID->
          newPc.removeStylesheetId(oldID)
        }
        newPc.setActiveStylesheet(null)
        newPc.setUseStylesheets(false)
        println "Piktogram ${pictoID} stylelist length: ${newPc.getStylesheetsController().getSelectedIds().size()}"
        byte[] newPCAsBytes = newPc.toDocument()
        f.getPictogramDVProxy().upload(CompressionUtil.deflate(newPCAsBytes),clientID,publicationID)
      }
    }
  }
} catch (Exception e) {
  try {
    f.getTransactionDVProxy().rollback();
  } catch (Exception e2) {
  }
  throw e;
} finally{
  f.getTransactionDVProxy().commit();
}
*/

println "--------------------- remove all stylesheets --------------------------------"
try{
  f.getTransactionDVProxy().open()
  def clientIDs = f.getClientDVProxy().getClientIDs()
  clientIDs.each{ clientID ->
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ publicationID ->
      def ids = f.getStyleDVProxy().getAllIDs(clientID,publicationID)
      ids.each{
        println "removing stylesheet ${it}"
        def proxy = f.getStyleDVProxy()
        proxy.markForDeletion(it)
      }
    }
  }
} catch (Exception e) {
  try {
    f.getTransactionDVProxy().rollback();
  } catch (Exception e2) {
  }
  throw e;
} finally{
  f.getTransactionDVProxy().commit();
}
try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}

