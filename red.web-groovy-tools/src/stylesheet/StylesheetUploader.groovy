package stylesheet
import distantvoices3.client.BasicClient
import distantvoices3.client.BasicClient
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.layout.pictogram.controller.PictogramController
import eu.red_web.library.compression.CompressionUtil
import eu.red_web.server.client.ClientFactory




def server = "redweb-testv4.me-intern.de"
def port = 8100
def user = "hermesb"
def passwd = "hermesb"
def userHome = System.getProperty("user.home")
def enc = "UTF-8"
def targetDir = "${userHome}/temp/css/${server}"

def boolean uploadStylesOnly = false
def boolean test = false

TWSerializer.initTypeAliases()

//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, port, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])


//if(!uploadStylesOnly){
//  println "--------------------- unlink stylesheets from pictos --------------------------------"
//  try {
//    f.getTransactionDVProxy().open()
//    def clientNames = []
//    new File(targetDir).eachDir { clientName ->
//      def clientID = f.getClientDVProxy().getClientID(clientName.name)
//      if(clientID != null){
//        def publicationDir = targetDir + File.separatorChar + clientName.name + File.separatorChar
//        println "publicationDir: ${new File(publicationDir).exists()} "
//        new File(publicationDir).eachDir{publicationName ->
//          def publicationID = f.getPublicationDVProxy().getObjectID(clientID, publicationName.name)
//          if(publicationID != null){
//            def allPictoIDs = f.getPictogramDVProxy().getIDs(publicationID)
//            allPictoIDs.each{ pictoID->
//              def pictoBytes = f.getPictogramDVProxy().download(pictoID)
//              def pictoXML = new String(CompressionUtil.inflate(pictoBytes),enc)
//              PictogramController newPc = this.getClass().getClassLoader().loadClass("eu.red_web.layout.pictogram.controller.PictogramController").newInstance()
//              newPc.fromDocument(pictoXML, true)
//              def oldIDList = newPc.getStyleIDs()
//              oldIDList.each{oldID->
//                newPc.removeStylesheetId(oldID)
//              }
//              newPc.setUseStylesheets(true)
//              println "Piktogram ${pictoID} stylelist length: ${newPc.getStylesheetsController().getSelectedIds().size()}"
//              byte[] newPCAsBytes = newPc.toDocument()
//              if(!test){
//                f.getPictogramDVProxy().upload(CompressionUtil.deflate(newPCAsBytes),clientID,publicationID)
//              }
//            }
//          }
//        }
//      }
//    }
//    f.getTransactionDVProxy().commit()
//  } catch (Exception e) {
//    try {
//      f.getTransactionDVProxy().rollback()
//    } catch (Exception e2) {
//    }
//    throw e
//  }
//}
//
//println "--------------------- upload stylesheets and link with pictos --------------------------------"
//
//try{
//  f.getTransactionDVProxy().open()
//  def clientNames = []
//  new File(targetDir).eachDir { clientName ->
//    def clientID = f.getClientDVProxy().getClientID(clientName.name)
//    if(clientID != null && clientName.name.compareTo("Mittelrhein-Verlag") == 0){
//      def publicationDir = targetDir + File.separatorChar + clientName.name + File.separatorChar
//      println "publicationDir: ${new File(publicationDir).exists()} "
//      new File(publicationDir).eachDir{publicationName ->
//        def publicationID = f.getPublicationDVProxy().getObjectID(clientID, publicationName.name)
//        if(publicationID != null && publicationName.name.compareTo("Tageszeitung") == 0){
//          def cssSrcDir = publicationDir + File.separatorChar + publicationName.name + File.separatorChar
//          new File(cssSrcDir).eachFileRecurse{file->
//            def fileName = file.absolutePath
//            if(fileName.endsWith(".css")){
//              def lastSlash = fileName.toString().lastIndexOf(File.separatorChar as String)
//              def styleName = fileName.substring(lastSlash + 1).replaceAll("_"," ")
//              styleName = styleName.replaceAll("\\.css", "")
//              def groupName = fileName.substring(cssSrcDir.size()-1,lastSlash)
//              //println "Style Gruppe: ${groupName} Sheet: ${styleName}"
//
//              String styleContent = file.getText(enc)
//              def fileNameWithoutExt = fileName.replaceAll("\\.css", "")
//              //parse stylesheet to get associated pictograms and stylesheets
//              def startString = "/*ifdef"
//              def endString = "enddef*/"
//              def pictoIDBlockStart = styleContent.indexOf(startString)
//              def pictoIDBlockEnd = styleContent.indexOf(endString)
//              def idsString = ""
//              if(pictoIDBlockStart >= 0 && pictoIDBlockEnd >=0){
//                def tempStart = pictoIDBlockStart + startString.length() + 1 < styleContent.length() ? pictoIDBlockStart + startString.length() + 1 : pictoIDBlockStart + startString.length()
//                def tempEnd = pictoIDBlockEnd + endString.length() + 1 < styleContent.length() ? pictoIDBlockEnd + endString.length() + 1 : pictoIDBlockEnd + endString.length()
//                idsString = styleContent.substring(tempStart, pictoIDBlockEnd)
//                styleContent = styleContent.substring(tempEnd)
//              }
//              def ids = idsString.split("\n")
//              if(ids.size() > 0){
//                def styleID = ids[0]
//                if(ids.size() > 1){
//                  styleContent = "@import \"${ids[1]}\";\n" + styleContent
//                }
//                //upload stylesheet
//                byte[] styleContentAsBytes = styleContent.getBytes(enc)
//                
//                //println "uploading stylesheet id: ${styleID} hex: ${ID.fromHexForm(styleID)} clientID: ${clientID} publicationID: ${publicationID} styleName: ${styleName} groupName: ${groupName}"//content:\n${styleContent}"
//                if(!test){
//                  f.getStyleDVProxy().upload(CompressionUtil.deflate(styleContentAsBytes),styleID,clientID,publicationID,styleName)
//                  f.getStyleDVProxy().setStyleGroup(styleID,groupName)
//                  f.getStyleDVProxy().setActive(styleID, true)
//                }
//                //link with pictogram
//                if(!uploadStylesOnly){
//                  ids.eachWithIndex{pictoID,index->
//                    if(index > 1){
//                      println "linking stylesheet with picto: ${pictoID}"
//                      byte[] pictoBytes = null
//                      try{
//                        pictoBytes = f.getPictogramDVProxy().download(pictoID)
//                      }catch(RemoteException){
//                        println "es existiert kein Pictogram mit der ID ${pictoID}"
//                      }
//                      if(pictoBytes != null && pictoBytes.size() > 0){
//                        def pictoXML = new String(CompressionUtil.inflate(pictoBytes),enc)
//                        println "picto id: ${pictoID} name: ${f.getPictogramDVProxy().getName(pictoID,0)}_${f.getPictogramDVProxy().getName(pictoID,1)}_${f.getPictogramDVProxy().getName(pictoID,2)} style: ${styleID}"
//                        PictogramController newPc = this.getClass().getClassLoader().loadClass("eu.red_web.layout.pictogram.controller.PictogramController").newInstance()
//                        newPc.fromDocument(pictoXML, true)
//                        newPc.setUseStylesheets(true)
//                        newPc.addSelectedStylesheetId(ID.fromHexForm(styleID))
//                        byte[] newPCAsBytes = newPc.toDocument()
//                        String pictoXMLWithStyles = new String(newPCAsBytes,enc)
//                        //println "styleID: ${styleID}:\n${pictoXMLWithStyles}"
//                        if(!test){
//                          f.getPictogramDVProxy().upload(CompressionUtil.deflate(newPCAsBytes),clientID,publicationID)
//                        }
//                      }
//                    }
//                  }
//                }
//              }
//            }
//          }
//        }
//      }
//    }
//  }
//}catch (Exception ex) {
//  ex.printStackTrace()
//  try {
//    f.getTransactionDVProxy().rollback()
//  } catch (Exception e2) {
//    e2.printStackTrace()
//  }
//}finally{
//  f.getTransactionDVProxy().commit()
//}

if(!uploadStylesOnly){
  println "--------------------- set first stylesheet active --------------------------------"
  try{
    f.getTransactionDVProxy().open()
    def clientNames = []
    new File(targetDir).eachDir { clientName ->
      def clientID = f.getClientDVProxy().getClientID(clientName.name)
      if(clientID != null){
        def publicationDir = targetDir + File.separatorChar + clientName.name + File.separatorChar
        println "publicationDir: ${new File(publicationDir).exists()} "
        new File(publicationDir).eachDir{publicationName ->
          def publicationID = f.getPublicationDVProxy().getObjectID(clientID, publicationName.name)
          if(publicationID != null){
            allPictoIDs = f.getPictogramDVProxy().getIDs(publicationID)
            allPictoIDs.each{ pictoID->
              def pictoBytes = f.getPictogramDVProxy().download(pictoID)
              def pictoXML = new String(CompressionUtil.inflate(pictoBytes),enc)
              PictogramController newPc = this.getClass().getClassLoader().loadClass("eu.red_web.layout.pictogram.controller.PictogramController").newInstance()
              newPc.fromDocument(pictoXML, true)
              def selStyleIds = newPc.getStyleIDs()
              if(selStyleIds.length > 0){
                newPc.getStylesheetsController().setActivated(selStyleIds[0])
                println "Piktogram ${pictoID} active style: ${newPc.getStylesheetsController().getActivatedStylesheetId()}"
              }
              newPc.setUseStylesheets(true)
              byte[] newPCAsBytes = newPc.toDocument()
              String pictoXMLNew = new String(newPCAsBytes,enc)
              //println "uploading pictogram ${pictoID}\n${pictoXMLNew}"
              if(!test){
                f.getPictogramDVProxy().upload(CompressionUtil.deflate(newPCAsBytes),clientID,publicationID)
              }
            }
          }
        }
      }
    }
    f.getTransactionDVProxy().commit()
  } catch (Exception e) {
    try {
      f.getTransactionDVProxy().rollback()
    } catch (Exception e2) {
    }
    throw e
  }
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}

