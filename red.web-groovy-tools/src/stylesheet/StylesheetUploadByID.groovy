package stylesheet

import distantvoices3.client.BasicClient
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.library.compression.CompressionUtil
import eu.red_web.server.client.ClientFactory



def server = "neptun.red-web.lan"
def user = "hermesb"
def passwd = "hermesb"
def userHome = System.getProperty("user.home")
def enc = "UTF-8"
def cssSrcDir =  ""
def clientID = ""
def publicationID = ""

if(Version.APPLICATION_VERSION_STRING.startsWith("4.")){
  TWSerializer.initTypeAliases()
}

//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, 8100, user, null);
ClientFactory f = new ClientFactory(cl);
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null);
if (keys[1]==null)
  throw new Exception(keys[3]);

cl.setPassword(keys[1]);



println "--------------------- upload stylesheets --------------------------------"

int i = 1;
def file = new File("${userHome}.redweb/${server}/filestore/Mittelrhein-Verlag/Tagezeitung/style/${stylesheetID}.css")
String styleContent = file.getText(enc);
def startString = "/*start picto ids"
def endString = "end picto ids*/"
println "updating style: ${styleID}"
byte[] styleContentAsBytes = styleContent.getBytes(enc)
try{
  f.getTransactionDVProxy().open()
  f.getStyleDVProxy().upload(CompressionUtil.deflate(styleContentAsBytes),stylesheetID,clientID,publicationID,styleID);
  f.getTransactionDVProxy().commit();
} catch (Exception e) {
  try {
    f.getTransactionDVProxy().rollback();
  } catch (Exception e2) {
  }
  throw e;
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}
