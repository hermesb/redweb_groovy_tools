package macro

import distantvoices3.client.BasicClient
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.server.client.ClientFactory


def server = "redweb4.local"
def user = "hermesb"
def passwd = "hermesb"

def enc = "UTF-8"

TWSerializer.initTypeAliases()


BasicClient cl = new BasicClient(server, 8104, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])

try{
  f.getTransactionDVProxy().open()
  def clientIDs = f.getClientDVProxy().getClientIDs()
  clientIDs.each{ clientID ->
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ objectID ->
      def objectName = f.getPublicationDVProxy().getName(objectID)
      println "${objectName}"
        def macroIDs = f.getMacroDVProxy().getAllMacroIDs(objectID,true,true)
        macroIDs.each{ id->
          def macroName = f.getMacroDVProxy().getName(id)
          def macroGroup = f.getMacroDVProxy().getGroup(id)
          def macroActive = f.getMacroDVProxy().isActive(id)
          def macroDeleted = f.getMacroDVProxy().isMarkedForDeletion(id)
          println "${macroGroup}/${macroName}\t${id}: active: ${macroActive} marked for deletion: ${macroDeleted}"
        }
    }
  }
  f.getTransactionDVProxy().commit()
} catch (Exception e) {
  e.printStackTrace()
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}


