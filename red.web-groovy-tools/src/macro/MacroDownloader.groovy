package macro

import distantvoices3.client.BasicClient
import eu.red_web.app.core.Version
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.server.client.ClientFactory

def server = "rw4-users-1.red-web.org"
def port = 8100
def user = "redweb"
def passwd = "redweb"
def enc = "UTF-8"
def targetDir = "/Users/hermesb/temp/macros/${server}"

if(Version.APPLICATION_VERSION_STRING.startsWith("4.")){
  TWSerializer.initTypeAliases()
}

BasicClient cl = new BasicClient(server, port, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])

//def pattern = "api\\.toUppercase"
//println pattern
//Pattern labelPattern = Pattern.compile(pattern)

try{
  f.getTransactionDVProxy().open()
  def clientIDs = f.getClientDVProxy().getClientIDs()
  clientIDs.each{ clientID ->
    def clientName = f.getClientDVProxy().getName(clientID)
    println "client id: ${clientID} name: ${clientName}"
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ objectID ->
      if(true || objectID.equals("98d3e")){
        def objectName = f.getPublicationDVProxy().getName(objectID)
        println "\tobject id: ${objectID} name: ${objectName}"
        SortedSet<String> labelSet = new TreeSet<String>()
        def allMacroIDs = f.getMacroDVProxy().getAllMacroIDs(objectID,false,false)
        allMacroIDs.each{macroID->
            def macroBytes = f.getMacroDVProxy().getMacro(macroID)
            def macroContent = new String(macroBytes,enc)
            def name = f.getMacroDVProxy().getName(macroID)
            def group = f.getMacroDVProxy().getGroup(macroID)
    
            group = group.replaceAll("\\\\" ,"#backslash#")
            group = group.replaceAll("\\/" ,"#slash#")
            group = group.replaceAll("<","#lower#")
            group = group.replaceAll(">","#larger#")
    
            name = name.replaceAll("\\\\","#backslash#")
            name = name.replaceAll("\\/","#slash#")
            name = name.replaceAll("<","#lower#")
            name = name.replaceAll(">","#larger#")
            
//            Matcher m = labelPattern.matcher(macroContent)
//            if(m.find(0)){
              def macroDir = "${targetDir}${File.separator}${clientName}${File.separator}${objectName}${File.separator}${group}"
              def macroFile = "${macroDir}${File.separator}${name}.js"
              println "backup macro to ${macroFile}"
              org.apache.commons.io.FileUtils.forceMkdir(new File(macroDir))
              def makroFile = new File(macroFile)
              makroFile.write(macroContent, enc)
//            }
          }
        }
    }
  }
  f.getTransactionDVProxy().commit()
} catch (Exception e) {
  try {
    f.getTransactionDVProxy().rollback()
  } catch (Exception e2) {
  }
  throw e
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}


