package macro

import distantvoices3.client.BasicClient
import eu.red_web.server.client.ClientFactory

/*
 *  Für Server die bereits auf 3.5 aktualisiert wurden, bei denen aber das Encoding der Makros nicht angepasst wurde!!!
 */


def server = "127.0.0.1"
def port = 8100
def user = "rw3support"
def passwd = 'gummipanzer01'
def sourceEnc = "ISO-8859-1"
//def sourceEnc = "x-MacRoman"
def targetEnc = "UTF-8"
def convertMacros = true
def convertScripts = false

//if(Version.APPLICATION_VERSION_STRING.startsWith("4.")){
//  TWSerializer.initTypeAliases()
//}

BasicClient cl = new BasicClient(server, port, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null){
  throw new Exception(keys[3])
}
cl.setPassword(keys[1])


try{
  f.getTransactionDVProxy().open()
  if(convertMacros){
    def clientIDs = f.getClientDVProxy().getClientIDs()
    clientIDs.each{ clientID ->
      def clientName = f.getClientDVProxy().getName(clientID)
      println "client id: ${clientID} name: ${clientName}"
      def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
      objectIDs.each{ objectID ->
        def objectName = f.getPublicationDVProxy().getName(objectID)
        println "\tobject id: ${objectID} name: ${objectName}"
        def allMacroIDs = f.getMacroDVProxy().getAllMacroIDs(objectID,false,false)
        allMacroIDs.each{macroID->
          byte[] macroBytes = f.getMacroDVProxy().getMacro(macroID)
          String macroContent = new String(macroBytes,sourceEnc)
          byte[] tempBytes = macroContent.getBytes(targetEnc)
          String tempContent = new String(tempBytes,targetEnc)
          /*
          if(sourceEnc.equals("x-MacRoman")){
            boolean isoFlag = false
            //ME only
            if(tempContent.contains("") || tempContent.contains("")  || tempContent.contains("¸") || tempContent.contains("") || tempContent.contains("÷") || tempContent.contains("") || tempContent.contains("?")){
              isoFlag = true
              macroContent = new String(macroBytes,"ISO-8859-1");
              tempBytes = macroContent.getBytes(targetEnc);
              tempContent = new String(tempBytes,targetEnc);
            }
          }
          */
          def name = f.getMacroDVProxy().getName(macroID)
          def description = f.getMacroDVProxy().getDescription(macroID)
          def group = f.getMacroDVProxy().getGroup(macroID)
          println "\n--------------------------------------------------------------------------------\nMacro ${macroID}:\n${tempContent}"
          //f.getMacroDVProxy().upload(tempBytes,macroID,clientID,objectID,name,description,group);
        }
      }
    }
  }
  /*
  if(convertScripts){
    def allScriptIDs = f.getApplicationScriptDVProxy().getAllIDs(null, null)
    allScriptIDs.each{scriptID->
      byte[] scriptBytes = f.getApplicationScriptDVProxy().getApplicationScript(scriptID)
      String scriptContent = new String(scriptBytes,sourceEnc)
      byte[] tempBytes = scriptContent.getBytes(targetEnc)
      String tempContent = new String(tempBytes,targetEnc)
      if(sourceEnc.equals("x-MacRoman")){
        boolean isoFlag = false
        //ME only
        if(tempContent.contains("") || tempContent.contains("")  || tempContent.contains("¸") || tempContent.contains("") || tempContent.contains("÷") || tempContent.contains("") || tempContent.contains("?")){
          isoFlag = true
          scriptContent = new String(scriptBytes,"ISO-8859-1");
          tempBytes = scriptContent.getBytes(targetEnc);
          tempContent = new String(tempBytes,targetEnc);
        }
      }
      def name = f.getApplicationScriptDVProxy().getName(scriptID)
      def description = f.getApplicationScriptDVProxy().getDescription(scriptID)
      def group = f.getApplicationScriptDVProxy().getGroup(scriptID)
      println "\n--------------------------------------------------------------------------------\nScript ${scriptID}:\n${tempContent}"
      //f.getApplicationScriptDVProxy().upload(tempBytes,scriptID,clientID,objectID,name,description,group);
    }
  }
*/
  f.getTransactionDVProxy().commit();
} catch (Exception e) {
  try {
    f.getTransactionDVProxy().rollback();
  } catch (Exception e2) {
  }
  throw e;
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}

