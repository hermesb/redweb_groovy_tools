package macro

import distantvoices3.client.BasicClient
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.server.client.ClientFactory
import groovy.xml.MarkupBuilder


def server = "rw4.flz.gmbh"
def port = 8100
def user = "hermesb"
def passwd = "hermesb"
def enc = "UTF-8"

TWSerializer.initTypeAliases()


BasicClient cl = new BasicClient(server, 8100, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])


def macroFilter = [
  "Apostroph":["control shift NUMBER_SIGN","meta shift NUMBER_SIGN"],
  "Autor":["control shift K","meta shift K"],
  "fett":["control shift F","meta shift F"],
  "Fotohinweis":["control shift B","meta shift B"],
  "Absatz markieren":["control M","meta M"],
  "Einfaches_Abführungszeichen":["control alt NUMBER_SIGN","meta alt NUMBER_SIGN"],
  "raute am absatzanfang":["control R","meta R"],
  "02_Schließende_Doppelte_Abführung":["alt S","alt S"],
  "01_Doppeltes_Anführungszeichen":["alt A","alt A"]
    /*
  "kursiv":["F3","F3"],
  "ortsmarke":["F4","F4"],
  "einzug auf null":["control F5","meta F5"],
  "absatz markieren":["control shift A","meta shift A"],
  "zeichen tiefstellen":["control T","meta T"],
  "zeichen hochstellen":["control H","meta H"],
  "raute am absatzanfang":["control R","meta R"],
  "bildzeilen":["F9","F9"],
  "Autor":["alt A","alt A"],
  "Bildanlauf":["alt shift b","alt shift B"],
  "Blauer Balken":["control alt B","meta alt B"],
  "Blauer Balken mit Titel 14Pt":["alt B","alt B"],
  "Bliss gewöhnlich":["control alt R","control alt R"],
  "Bliss halbfett":["alt F","alt F"],
  "Frage":["alt shift F","alt shift F"],
  "Info":["alt shift I","alt shift I"],
  "Novel gewöhnlich":["alt N","alt N"],
  "Ortsmarke":["alt O","alt O"],
  "Rastergeviert":["alt Q","alt Q"],
  "Seitenverweis gleiche Zeile":["shift F6","shift F6"],
  "Seitenverweis nächste Zeile":["shift F7","shift F7"],
  "Spationieren":["alt F4","alt F4"],
  "Titel Leiste 14 Punkt":["alt T","alt T"],
  "Titel Leiste 9 Punkt":["alt K","alt K"],
  "Zentrieren_blocksatz":["alt I","alt I"],
  "Zentrieren_links":["alt 2","alt 2"],
  "Zentrieren_mittig":["alt 3","alt 3"],
  "Zentrieren_rechts":["alt 4","alt 4"],
  "Bliss_klein_halbfett":["alt shift X","alt shift X"],
  "Ergebnisse":["alt shift F7","alt shift F7"],
  "Liganamen":["alt shift F6","alt shift F6"],
  "Novel_gewoehnlich_klein":["alt shift Y","alt shift Y"],
*/
  ]

try{
  f.getTransactionDVProxy().open()

  def writer = new StringWriter()
  def xml = new MarkupBuilder(writer)
  xml.setDoubleQuotes(true)
  xml.category(name:'system-macros') {

    def clientIDs = f.getClientDVProxy().getClientIDs()
    clientIDs.each{ clientID ->
      //println "client id: ${clientID} name: ${f.getClientDVProxy().getName(clientID)}"
      def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
      objectIDs.each{ objectID ->
        def result = []
        def objectName = f.getPublicationDVProxy().getName(objectID)
        //println "\tobject id: ${objectID} name: ${objectName}"
        if(true || objectName.equals("OVS")){
          def macroIDs = f.getMacroDVProxy().getAllMacroIDs(objectID,false,false)
          macroIDs.each{ id->
            def macroName = f.getMacroDVProxy().getName(id)
            def macroGroup = f.getMacroDVProxy().getGroup(id)

            def key = macroFilter.keySet().find{filter-> macroName.toLowerCase().contains(filter.toLowerCase())}
            if(key != null && !key.isEmpty()){
              result += macroGroup + "###" + macroName + "###" + id + "###" + macroFilter[key][0] + "###" + macroFilter[key][1]
              //println result
            }
          }
          category(name:objectID) {
            mkp.comment "Publikation: ${objectName}"
            result.sort().each{
              def splitted = it.split("###")
              def macroGroup = splitted[0]
              def macroName = splitted[1]
              def macroID = splitted[2]
              def shortcut = splitted[3]
              def osxshortcut = splitted[4]
              category(name:macroID) {
                mkp.comment "Gruppe: ${macroGroup} Name:${macroName}"
                property(name:'osx-shortcut',value:osxshortcut)
                property(name:'default-shortcut',value:shortcut)
              }
            }
          }
        }
      }
    }
  }
  f.getTransactionDVProxy().commit()

  println writer
} catch (Exception e) {
  e.printStackTrace()
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}


