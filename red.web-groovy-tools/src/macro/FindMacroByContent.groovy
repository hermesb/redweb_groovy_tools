package macro
import distantvoices3.client.BasicClient
import eu.red_web.app.core.Version
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.server.client.ClientFactory


def server = "rw4.it4media.de"
def user = "hermesb"
def passwd = "hermesb"

def enc = "UTF-8"

if(Version.APPLICATION_VERSION_STRING.startsWith("4.")){
  TWSerializer.initTypeAliases()
}

//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, 8100, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])

println "--------------------- unlink stylesheets from pictos --------------------------------"
try{
  f.getTransactionDVProxy().open()
  def clientIDs = f.getClientDVProxy().getClientIDs()
  clientIDs.each{ clientID ->
    println "client id: ${clientID} name: ${f.getClientDVProxy().getName(clientID)}"
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ objectID ->
      println "\tobject id: ${objectID} name: ${f.getPublicationDVProxy().getName(objectID)}"
      def allMacroIDs = f.getMacroDVProxy().getAllMacroIDs(objectID,false,false)
      allMacroIDs.each{macroID->
        def macroBytes = f.getMacroDVProxy().getMacro(macroID)
        def macroContent = new String(macroBytes,enc)
        if(macroContent.contains("api.toUppercase()")){
          def name = f.getMacroDVProxy().getName(macroID)
          def group = f.getMacroDVProxy().getGroup(macroID)
          println "\t\tfound: ${group}/${name}"
        }
      }
    }
  }
  f.getTransactionDVProxy().commit()
} catch (Exception e) {
  try {
    f.getTransactionDVProxy().rollback()
  } catch (Exception e2) {
  }
  throw e
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}

