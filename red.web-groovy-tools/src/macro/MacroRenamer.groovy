package macro


import distantvoices3.client.BasicClient
import eu.red_web.app.core.Version
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.server.client.ClientFactory

//def server = "rw4-users-1.red-web.org"
def port = 8100
def user = "hermesb"
def enc = "UTF-8"
def targetDir = "/Users/hermesb/temp/macros/"

if(Version.APPLICATION_VERSION_STRING.startsWith("4.")){
  TWSerializer.initTypeAliases()
}

BasicClient cl = new BasicClient(server, port, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])


try{
  f.getTransactionDVProxy().open()
  def suffix = " CSS"
  def clientIDs = f.getClientDVProxy().getClientIDs()
  clientIDs.each{ clientID ->
    def clientName = f.getClientDVProxy().getName(clientID)
    println "client id: ${clientID} name: ${clientName}"
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ objectID ->
      def objectName = f.getPublicationDVProxy().getName(objectID)
      println "\tobject id: ${objectID} name: ${objectName}"
      if(objectName.equals("Tageszeitung")){
        def allMacroIDs = f.getMacroDVProxy().getAllMacroIDs(objectID,false,false)
        //Lauf 1: Umbenennen aller Makrogruppen die nicht mit " CSS" enden in " ALT" und auf nicht aktiv setzen 
        println "move to ALT ------------------------------------------------------------------"
        allMacroIDs.each{macroID->
          def name = f.getMacroDVProxy().getName(macroID)
          def group = f.getMacroDVProxy().getGroup(macroID)
          if(!group.equals("system") && !group.endsWith(suffix) && !group.endsWith(" ALT")){
            def msg = "found group: ${group}"
            group += " ALT"
            println "${msg} renamed to: ${group}"
            f.getMacroDVProxy().setGroup(macroID,group)
            f.getMacroDVProxy().setActive(macroID,false)
          }
        }
        //Lauf 2: Entfernen der Endung " CSS" aller Makrogruppen die mit " CSS" enden 
        println "\n\n\nremove suffix -----------------------------------------------------------------"
        allMacroIDs.each{macroID->
          def name = f.getMacroDVProxy().getName(macroID)
          def group = f.getMacroDVProxy().getGroup(macroID)
          if(!group.equals("system") && group.endsWith(suffix)){
            def index = group.lastIndexOf(suffix)
            if(index >= 0){
              def msg = "found group: ${group}"
              group = group.substring(0,index)
              println "${msg} renamed to: ${group}"
              f.getMacroDVProxy().setGroup(macroID,group)
            }
          }

        }
      }
    }
  }
  f.getTransactionDVProxy().commit()
} catch (Exception e) {
  try {
    f.getTransactionDVProxy().rollback()
  } catch (Exception e2) {
  }
  throw e
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}


