package macro
import distantvoices3.client.BasicClient
import distantvoices3.client.BasicClient
import eu.red_web.app.core.Version
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.server.client.ClientFactory




//def server = "neptun.red-web.lan"
def server = "127.0.0.1"
def port = 8100
def user = "hermesb"
def passwd = "hermesb"
def userHome = System.getProperty("user.home")
def enc = "UTF-8"
def targetDir = "${userHome}/temp/macros/${server}"


def boolean uploadStylesOnly = false
def boolean test = false

if(Version.APPLICATION_VERSION_STRING.startsWith("4.")){
  TWSerializer.initTypeAliases()
}

//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, port, user, null);
ClientFactory f = new ClientFactory(cl);
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null);
if (keys[1]==null)
  throw new Exception(keys[3]);

cl.setPassword(keys[1]);

println "--------------------- upload macros from folder --------------------------------"

try{
  f.getTransactionDVProxy().open()
  def clientNames = []
  new File(targetDir).eachDir { clientName ->
    def clientID = f.getClientDVProxy().getClientID(clientName.name)
    if(clientID != null){
      def publicationDir = targetDir + File.separatorChar + clientName.name + File.separatorChar
      println "publicationDir: ${new File(publicationDir).exists()} "
      new File(publicationDir).eachDir{publicationName ->
        def publicationID = f.getPublicationDVProxy().getObjectID(clientID, publicationName.name)
        if(publicationID != null){
          def allIDs = f.getMacroDVProxy().getAllMacroIDs(publicationID,true,true)
          def macroMap = [:]
          allIDs.each{macroID->
            def macroName = f.getMacroDVProxy().getName(macroID)
            def macroGroup = f.getMacroDVProxy().getGroup(macroID)
            macroMap[macroName + "_" + macroGroup] = macroID
          }
          def macroGroupDir = publicationDir + File.separatorChar + publicationName.name + File.separatorChar
          new File(macroGroupDir).eachDir{groupDir->
            def groupName = groupDir.name
            groupDir.eachFileRecurse{macroFile->
              def fileName = macroFile.name
              if(fileName.endsWith(".js")){
                String macroContent = macroFile.getText("ISO-8859-1");
                def macroName = fileName.replaceAll("\\.js", "")
                byte[] tempBytes = macroContent.getBytes(enc)
                def macroID = macroMap[macroName + "_" + groupName]
                println "uploading macro - id ${macroID} group ${groupName} name ${macroName}\n${macroContent}"
                macroID = f.getMacroDVProxy().upload(tempBytes,null,clientID,publicationID,macroName,"",groupName);
                f.getMacroDVProxy().setActive(macroID,true);
              }
            }
          }
        }
      }
    }
  }
}catch (Exception ex) {
  ex.printStackTrace()
  try {
    f.getTransactionDVProxy().rollback();
  } catch (Exception e2) {
    e2.printStackTrace()
  }
}finally{
  f.getTransactionDVProxy().commit()
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}

