package grid

import distantvoices3.client.BasicClient
import eu.red_web.server.client.ClientFactory


def server = "redweb-p.it4media.de"
def user = "hermesb"
def passwd = "hermesb"

def enc = "UTF-8"

BasicClient cl = new BasicClient(server, 8100, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])


try{
  f.getTransactionDVProxy().open()
  def clientIDs = f.getClientDVProxy().getClientIDs()
  clientIDs.each{ clientID ->
    println "client id: ${clientID} name: ${f.getClientDVProxy().getName(clientID)}"
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ objectID ->
      println "\tobject id: ${objectID} name: ${f.getPublicationDVProxy().getName(objectID)}"
      def gridIDs = f.getGridDVProxy().getIDs(objectID)
      gridIDs.each{ gridID->
        def gridName = f.getGridDVProxy().getName(gridID,"de")
        def isDefault = f.getGridDVProxy().isDefault(gridID)
        def isActive = f.getGridDVProxy().isActive(gridID)
        def isDeleted = f.getGridDVProxy().isDeleted(gridID)
        def isMarkedForDeletion = f.getGridDVProxy().isMarkedForDeletion(gridID)
        println "\t\tgrid id: ${gridID} name: ${gridName} isdefault: ${isDefault} markedForDeletion: ${isMarkedForDeletion} isActive: ${isActive} isDeleted: ${isDeleted}"
//        if(gridID.equals("41fc846e04ab4c30") || gridID.equals("6cdabea80f81e77") || gridID.equals("24d463ce8249884e") || gridID.equals("6e88fe159045140d") || gridID.equals("69a816d93b22e323") || gridID.equals("3031d31b322e05f3") || gridID.equals("848da2682080c4e") || gridID.equals("64d7e01646b5b736") || gridID.equals("2ce2c068dfe5befb") || gridID.equals("531fd8d02be492e8")){
//          f.getGridDVProxy().setAsDefault(gridID, true)
//          println "set as default ${gridID}"
//        }
      }
    }
  }
  f.getTransactionDVProxy().commit()
} catch (Exception e) {
  e.printStackTrace()
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}


