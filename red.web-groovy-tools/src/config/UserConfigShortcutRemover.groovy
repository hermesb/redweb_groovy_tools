package config
import distantvoices3.client.BasicClient
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.server.client.ClientFactory
import groovy.xml.StreamingMarkupBuilder


//def server = "redweb-t.it4media.de"
//def server = "127.0.01"
def user = "hermesb"
def passwd = "hermesb"
def userHome = System.getProperty("user.home")
def enc = "UTF-8"
def targetDir = "${userHome}/temp/userconfig/${server}"


TWSerializer.initTypeAliases()

//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, 8100, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])

println "--------------------- unlink stylesheets from pictos --------------------------------"
try{
  f.getTransactionDVProxy().open()

  def availableUserConfigurations = f.getApplicationConfigurationDVProxy().getAvailableUserConfigurations()
  availableUserConfigurations.each{userID->
    println "user: ${userID}"
    def userConfigBytes = f.getApplicationConfigurationDVProxy().getUserConfigurationContents(userID)
    def userConfig = new String(userConfigBytes,enc)
    def fileBackup = new File("${targetDir}/backup/${userID}.xml")
    fileBackup.write(userConfig, enc)
    //user config shortcuts entfernen
    def xml = new XmlSlurper().parseText(userConfig)

    def userName = xml.category?.find{it['@name'] == '_meta_'}?.property.find{it['@name']  == 'user'}?.@value.text()
    if(userName != null){
      println "user name: ${userName}"
    }

    def categoriesWhereToRemoveChildren = ["shortcut-preferences","user-shortcuts","system-macros"]
    categoriesWhereToRemoveChildren.each{categoryName->
      println "\tsearching: ${categoryName}"
      xml.category.findAll{it.@name.text().equals(categoryName)}.each{node->
        println "\t\tfound node: ${node.@name.text()}"
        node.children().each{child ->
          println "\t\t\tremove child: ${child.@name.text()}"
          child.replaceNode{}}
      }
    }

    def newUserConfig = new StreamingMarkupBuilder().bind {
      mkp.yield xml
    }.toString()

    def file = new File("${targetDir}/${userID}.xml")
    file.write(newUserConfig, enc)

    byte[] newUserConfigBytes = newUserConfig.getBytes(enc)
    //f.getApplicationConfigurationDVProxy().storeUserConfigurationContents(userID,newUserConfigBytes)
  }
  f.getTransactionDVProxy().commit()
} catch (Exception e) {
  try {
    f.getTransactionDVProxy().rollback()
  } catch (Exception e2) {
  }
  throw e
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}


