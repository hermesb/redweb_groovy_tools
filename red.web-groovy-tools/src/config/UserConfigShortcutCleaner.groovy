package config
import distantvoices3.client.BasicClient
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.server.client.ClientFactory
import groovy.xml.StreamingMarkupBuilder


//def server = "rw4-users-1.red-web.org"
//def port = 8100
def server = "redweb4.local"
def port = 8104
def user = "hermesb"
def passwd = "hermesb"
def userHome = System.getProperty("user.home")
def enc = "UTF-8"
def targetDir = "${userHome}/temp/userconfig/${server}"


TWSerializer.initTypeAliases()

//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, port, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])

def validMacroIDs = [:]
try{
  f.getTransactionDVProxy().open()
  def clientIDs = f.getClientDVProxy().getClientIDs()
  clientIDs.each{ clientID ->
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ objectID ->
      def objectName = f.getPublicationDVProxy().getName(objectID)
      println "${objectName}"
        def macroIDs = f.getMacroDVProxy().getAllMacroIDs(objectID,false,false)
        macroIDs.each{ id->
          def macroName = f.getMacroDVProxy().getName(id)
          def macroGroup = f.getMacroDVProxy().getGroup(id)
          validMacroIDs[id] = "${macroGroup}/${macroName}" 
        }
    }
  }
  f.getTransactionDVProxy().commit()
} catch (Exception e) {
  e.printStackTrace()
}

def validUserNames = [:]

//validUserNames.put("joeckela","joeckela")
validUserNames.put("hermesb","hermesb")

try{
  f.getTransactionDVProxy().open()

  def availableUserConfigurations = f.getApplicationConfigurationDVProxy().getAvailableUserConfigurations()
  availableUserConfigurations.each{userID->
    def userConfigBytes = f.getApplicationConfigurationDVProxy().getUserConfigurationContents(userID)
    def userConfig = new String(userConfigBytes,enc)
    def xml = new XmlSlurper().parseText(userConfig)

    def userName = xml.category?.find{it['@name'] == '_meta_'}?.property.find{it['@name']  == 'user'}?.@value.text()
    if(userName != null && validUserNames.get(userName) != null){
      println "user name: ${userName} id: ${userID}"
      def fileBackup = new File("${targetDir}/backup/${userID}.xml")
      fileBackup.write(userConfig, enc)
      def categoryName = "user-shortcuts"
      println "searching: ${categoryName}"
      xml.category.findAll{it.@name.text().equals(categoryName)}.each{node1->
        node1.children().each{node2->
          node2.children().each{node3->
            def macroID = node3.@name.text()
            if(validMacroIDs.get(macroID) == null){
              println "invalid macro shortcut: ${macroID}"
              node3.replaceNode{}
            }else{
              println "valid macro shortcut: ${macroID}: ${node2}"
            }
          }
        }
      }
      def newUserConfig = new StreamingMarkupBuilder().bind {
        mkp.yield xml
      }.toString()
  
      def file = new File("${targetDir}/${userID}.xml")
      file.write(newUserConfig, enc)
  
      byte[] newUserConfigBytes = newUserConfig.getBytes(enc)
      //f.getApplicationConfigurationDVProxy().storeUserConfigurationContents(userID,newUserConfigBytes)
    }
  }
  f.getTransactionDVProxy().commit()
} catch (Exception e) {
  try {
    f.getTransactionDVProxy().rollback()
  } catch (Exception e2) {
  }
  throw e
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}


