package config
import distantvoices3.client.BasicClient
import eu.red_web.server.client.ClientFactory


def server = "rw4-users-1.red-web.org"
def user = "redweb"
def passwd = "redweb"
def userHome = System.getProperty("user.home")
def enc = "UTF-8"
def targetDir = "${userHome}/temp/userconfig/${server}"


//TWSerializer.initTypeAliases()

//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, 8100, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])

try{
  f.getTransactionDVProxy().open()

  def availableUserConfigurations = f.getApplicationConfigurationDVProxy().getAvailableUserConfigurations()
  availableUserConfigurations.each{userID->
    def userConfigBytes = f.getApplicationConfigurationDVProxy().getUserConfigurationContents(userID)
    def userConfig = new String(userConfigBytes,enc)
    //println "downloading user ${userID}"
    def xml = new XmlSlurper().parseText(userConfig)
    def userName = xml.category?.find{it['@name'] == '_meta_'}?.property.find{it['@name']  == 'user'}?.@value.text()
    println userName
    if(userName != null){
      if(userName.equalsIgnoreCase("kauera")){
        println "user name: ${userName}: ${userID}"
        def file = new File("${targetDir}/${userID}.xml")
        file.write(userConfig, enc)
      }
    }
  }
  f.getTransactionDVProxy().commit()
} catch (Exception e) {
  try {
    f.getTransactionDVProxy().rollback()
  } catch (Exception e2) {
  }
  throw e
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}


