package config
import distantvoices3.client.BasicClient
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.library.compression.CompressionUtil
import eu.red_web.server.client.ClientFactory


//def server = "192.168.163.128"
def server = "neptun.red-web.lan"
def user = "hermesb"
def passwd = "hermesb"
def userHome = System.getProperty("user.home")
def enc = "UTF-8"
def targetDir = "${userHome}/temp/"

TWSerializer.initTypeAliases()

//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, 8100, user, null);
ClientFactory f = new ClientFactory(cl);
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null);
if (keys[1]==null)
  throw new Exception(keys[3]);

cl.setPassword(keys[1]);

println "--------------------- unlink stylesheets from pictos --------------------------------"
try{
  f.getTransactionDVProxy().open()
  def sysConfigBytes = f.getApplicationConfigurationDVProxy().downloadSearchConfiguration()
  def sysConfig = new String(CompressionUtil.inflate(sysConfigBytes),enc);
  def file = new File("${targetDir}/search-config.xml")
  file.write(sysConfig, enc)
  f.getTransactionDVProxy().commit()
} catch (Exception e) {
  try {
    f.getTransactionDVProxy().rollback();
  } catch (Exception e2) {
  }
  throw e;
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}


