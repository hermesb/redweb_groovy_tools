package config
import distantvoices3.client.BasicClient
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.library.compression.CompressionUtil
import eu.red_web.server.client.ClientFactory


//def server = "192.168.163.128"
//def server = "redweb-t.it4media.de"
def server = "127.0.0.1"
def user = "hermesb"
def passwd = "hermesb"
def userHome = System.getProperty("user.home")
def enc = "UTF-8"
def targetDir = "${userHome}/temp/config/${server}"

TWSerializer.initTypeAliases()

//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, 8100, user, null);
ClientFactory f = new ClientFactory(cl);
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null);
if (keys[1]==null)
  throw new Exception(keys[3]);

cl.setPassword(keys[1]);

println "uploading config to: ${server}"
System.in.withReader {
  print "do you really want to upload? enter y:"
  def input = it.readLine()
  if(!input.equalsIgnoreCase("y")){
    println "canceled";
    System.exit(0);
  }
}
println "uploading now"
try{
  f.getTransactionDVProxy().open()
  def file = new File("${targetDir}/redweb.xml")
  byte[] sysConfigBytes = file.getText(enc).getBytes(enc);
  byte[] sysConfigDeflBytes = CompressionUtil.deflate(sysConfigBytes)
  f.getApplicationConfigurationDVProxy().uploadSystemConfiguration(sysConfigDeflBytes)
  f.getTransactionDVProxy().commit()
  println "uploading finished"
} catch (Exception e) {
  try {
    f.getTransactionDVProxy().rollback();
  } catch (Exception e2) {
    e2.printStackTrace();
  }
  throw e;
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}

