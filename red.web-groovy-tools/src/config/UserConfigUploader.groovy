package config
import distantvoices3.client.BasicClient
import eu.red_web.server.client.ClientFactory


//def server = "rw4-users-1.red-web.org"
def user = "redweb"
def passwd = "redweb"
def userHome = System.getProperty("user.home")
def enc = "UTF-8"
def targetDir = "${userHome}/temp/userconfig/${server}"
def userID = "618b418bf978974"

//TWSerializer.initTypeAliases()

//MUST BE RUN AS JAVA APPLICATION. ELSE CLASS CAST EXCEPTIONS WILL OCCUR DUE TO DIFFERENT CLASS LOADERS
BasicClient cl = new BasicClient(server, 8100, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])
cl.setPassword(keys[1])


println "uploading config to: ${server}"
System.in.withReader {
  print "do you really want to upload? enter y:"
  def input = it.readLine()
  if(!input.equalsIgnoreCase("y")){
    println "canceled"
    System.exit(0)
  }
}
println "uploading now"

try{
  f.getTransactionDVProxy().open()

  def file = new File("${targetDir}/${userID}.xml")
  def fileText = file.getText(enc)
  println "config content:\n${fileText}"
  byte[] newUserConfigBytes = fileText.getBytes(enc)
  f.getApplicationConfigurationDVProxy().storeUserConfigurationContents(userID,newUserConfigBytes)
  f.getTransactionDVProxy().commit()
  println "uploading finished"
} catch (Exception e) {
  try {
    f.getTransactionDVProxy().rollback()
  } catch (Exception e2) {
  }
  throw e
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}


