package rws

import groovy.xml.MarkupBuilder

def csvDirStr = "/Users/hermesb/Documents/MRV/Unterlagen/börsentabellen/csv/"
def rwsDir = "/Users/hermesb/Desktop/"

def csvDir = new File(csvDirStr).eachFile {file->
  if(file.name.startsWith("Indizes-") && file.absolutePath.endsWith(".csv")){
    def fileNameWithoutExt = file.name.substring(0,file.name.length()-4)
    def writer = new StringWriter()
    def xml = new MarkupBuilder(writer)
    xml.doc(){
      info(apperversion:"2.4.0.0",layoutversion:"2.1.0.0")
      def row=1
      String[] lines = file.getText("UTF-8").split("\n")
      def lineCount = lines.size();
      lines.each{line->
        println line
        def items = line.split("\\|")
        println "item: ${items}"
        if(items.size() == 8){
          def region = items[0]
          def name = items[1]
          def val1 = items[2]
          def prvs = items[3]
          def chng1 = items[4]
          def chng2 = items[5]
          def high = items[6]
          def low  = items[7]
          if(row == 1){
            paragraph(){
              content(){
                text(value:"%VGT%",mactype:"Wirtschaft",macname:"Tabelle Rand links")
                text(value:"${fileNameWithoutExt.substring(8)}",mactype:"Wirtschaft",macname:"Tabelle Region")
                text(value:"%TAB%")
                text(value:"%VGT%",mactype:"Wirtschaft",macname:"Tabelle Trennlinie")
                text(value:"%TAB%")
                text(value:"%VGT%",mactype:"Wirtschaft",macname:"Tabelle Rand rechts")
                text(value:"%TAB%")
                text(value:"%VGT%",mactype:"Wirtschaft",macname:"Tabelle Rand rechts")
                text(value:"%TAB%")
                text(value:"%VGT%",mactype:"Wirtschaft",macname:"Tabelle Rand rechts")
                text(value:"%TAB%")
                text(value:"%VGT%",mactype:"Wirtschaft",macname:"Tabelle Rand rechts")
                text(value:"%TAB%")
                text(value:"%VGT%",mactype:"Wirtschaft",macname:"Tabelle Rand rechts")
                text(value:"%TAB%")
                text(value:"%VGT%",mactype:"Wirtschaft",macname:"Tabelle Rand rechts")
                text(value:"%TAB%")
                text(value:"%VGT%",mactype:"Wirtschaft",macname:"Tabelle Rand rechts")
              }
            }
          }
          paragraph(){
            content(){
              text(value:"%VGT%",mactype:"Wirtschaft",macname:"Tabelle Rand links")
              if(row < lineCount){
                text(value:"${region}")
              }else{
                text(value:"${region}",mactype:"Wirtschaft",macname:"Tabelle Trennlinie")
              }
              text(value:"%TAB%")
              text(value:"%VGT%",mactype:"Wirtschaft",macname:"Tabelle Rand links")
              text(value:"${name}")
              text(value:"%TAB%")
              text(value:"%VGT%",mactype:"Wirtschaft",macname:"Tabelle Rand rechts")
              text(value:"%TAB%")
              text(value:"${val1}")
              text(value:"%VGT%",mactype:"Wirtschaft",macname:"Tabelle Rand rechts")
              text(value:"%TAB%")
              text(value:"${prvs}")
              text(value:"%VGT%",mactype:"Wirtschaft",macname:"Tabelle Rand rechts")
              text(value:"%TAB%")
              text(value:"${chng1}")
              text(value:"%VGT%",mactype:"Wirtschaft",macname:"Tabelle Rand rechts")
              text(value:"%TAB%")
              text(value:"${chng2}")
              text(value:"%VGT%",mactype:"Wirtschaft",macname:"Tabelle Rand rechts")
              text(value:"%TAB%")
              text(value:"${high}")
              text(value:"%VGT%",mactype:"Wirtschaft",macname:"Tabelle Rand rechts")
              text(value:"%TAB%")
              text(value:"${low}")
              text(value:"%VGT%",mactype:"Wirtschaft",macname:"Tabelle Rand rechts")
            }
          }
          ++row
        }
      }
    }
    //println writer.toString()
    File rwsFile = new File(rwsDir + fileNameWithoutExt + ".rws");
    rwsFile.write(writer.toString(), "UTF-8")
  }
}
