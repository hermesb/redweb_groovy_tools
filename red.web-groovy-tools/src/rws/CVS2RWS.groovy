package rws

import groovy.xml.MarkupBuilder

def csvDirStr = "/Users/hermesb/Documents/MRV/Unterlagen/börsentabellen/csv/"
def rwsDir = "/Users/hermesb/Desktop/"

def csvDir = new File(csvDirStr).eachFile {file->
//  println file.absolutePath
//  println file.name
  if(file.name.startsWith("Börsen-") && file.absolutePath.endsWith(".csv")){
    def writer = new StringWriter()
    def xml = new MarkupBuilder(writer)
    xml.doc(){
      info(apperversion:"2.4.0.0",layoutversion:"2.1.0.0")
      def row=1
      file.splitEachLine("\\|","UTF-8") {items->
        if(items.size() == 6){
          def name = items[0]
          def val1 = items[1]
          def prvs = items[2]
          def chng = items[3]
          def high = items[4]
          def low  = items[5]
          //println "name: ${name} val1: ${val1} prvs: ${prvs} chng: ${chng} high: ${high} low: ${low}"
          if(row == 1){
            paragraph(type:"soft"){
              content(){
                text(value:"%TAB%")
                text(value:"%TAB%",mactype:"Wirtschaft",macname:"Tabelle Trennlinie")
                text(value:"%TAB%%TAB%${chng}%TAB%${high}%TAB%${low}",mactype:"Wirtschaft",macname:"Tabelle fett")
              }
            }
          }
          if(row > 1){
            paragraph(){
              content(){
                if(row == 2){
                  text(value:"%TAB%${name}",mactype:"Wirtschaft",macname:"Tabelle Ort")
                  text(value:"%TAB%${val1}%TAB%${prvs}%TAB%%%TAB%2012%TAB%2012",mactype:"Wirtschaft",macname:"Tabelle fett")
                }else{
                  text(value:"%TAB%${name}%TAB%${val1}%TAB%${prvs}%TAB%${chng}%TAB%${high}%TAB%${low}")
                }
              }
            }
          }
          if(row == 2){
            paragraph(){
              content(){
              }
            }
          }
        }
        ++row
      }
    }
    //println writer.toString()
    File rwsFile = new File(rwsDir + file.name.replaceAll("\\.cvs","") + ".rws");
    rwsFile.write(writer.toString(), "UTF-8")
  }
}
