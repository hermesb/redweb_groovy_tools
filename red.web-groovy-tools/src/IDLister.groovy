import distantvoices3.client.BasicClient
import eu.red_web.server.client.ClientFactory


def server = "redweb4.local"
def port = 8104
def user = "hermesb"
def passwd = "hermesb"
def enc = "UTF-8"

//if(Version.APPLICATION_VERSION_STRING.startsWith("3.9")){
//  TWSerializer.initTypeAliases()
//}

BasicClient cl = new BasicClient(server, port, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])


try{
  f.getTransactionDVProxy().open()
  def clientIDs = f.getClientDVProxy().getClientIDs()
        println "---------------- articlelayouts ------------------"

    clientIDs.each{ clientID ->
    println "client name: ${f.getClientDVProxy().getName(clientID).padRight(36)} id: ${clientID}"
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ objectID ->
      println " object name: ${f.getPublicationDVProxy().getName(objectID).padRight(35)} id: ${objectID}"

      def result = []
      def pictoIDs = f.getPictogramDVProxy().getIDs(objectID)
      pictoIDs.each{ id->
        def name1 = f.getPictogramDVProxy().getName(id,0)
        def name2 = f.getPictogramDVProxy().getName(id,1)
        def name3 = f.getPictogramDVProxy().getName(id,2)
        name1 = name1 != null ? name1 : ""
        name2 = name2 != null ? name2 : ""
        name3 = name3 != null ? name3 : ""
        def name = "${name1}\\${name2}\\${name3}"
        def item = "   ${name.padRight(80)}\tid: ${id}"
        result += item
      }
      result.sort().each{
        println it
      }
    }
  }

  println "---------------- pagelayouts ------------------"
  clientIDs.each{ clientID ->
    println "client id: ${clientID} name: ${f.getClientDVProxy().getName(clientID)}"
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ objectID ->
      println "\tobject id: ${objectID} name: ${f.getPublicationDVProxy().getName(objectID)}"
      def pagelayoutIDs = f.getPagelayoutDVProxy().getIDs(objectID)
      pagelayoutIDs.each{ pagelayoutID->
        def pagelayoutName = f.getPagelayoutDVProxy().getName(pagelayoutID,"de")
        def isDefault = f.getPagelayoutDVProxy().isDefault(pagelayoutID)
        println "\t\tpagelayout id: ${pagelayoutID} name: ${pagelayoutName} isdefault: ${isDefault}"
      }
    }
  }


  
  println "---------------- grids ------------------"
  clientIDs.each{ clientID ->
    println "client id: ${clientID} name: ${f.getClientDVProxy().getName(clientID)}"
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ objectID ->
      println "\tobject id: ${objectID} name: ${f.getPublicationDVProxy().getName(objectID)}"
      def gridIDs = f.getGridDVProxy().getIDs(objectID)
      gridIDs.each{ gridID->
        def gridName = f.getGridDVProxy().getName(gridID,"de")
        def isDefault = f.getGridDVProxy().isDefault(gridID)
        println "\t\tgrid id: ${gridID} name: ${gridName} isdefault: ${isDefault}"
      }
    }
  }

  println "---------------- ressorts ------------------"
  clientIDs.each{ clientID ->
    //println "client: ${f.getClientDVProxy().getName(clientID)} id: ${clientID}"
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ objectID ->
      println "Publikation: ${f.getPublicationDVProxy().getName(objectID)} id: ${objectID}"
      def result = [:]
      def tree = [:]

      //println "\t\tdepartments"
      def depIDs = f.getDepartmentDVProxy().getIDs(objectID)
      depIDs.each{ depID->
        def depName = f.getDepartmentDVProxy().getName(depID,"de")
        def parentDep = f.getDepartmentDVProxy().getParentDepartment(depID)
        if(parentDep == null){
          parentDep = ""
        }
        //def item = "${depName.padRight(30)}id: ${depID.padLeft(20)}"
        def item = [depID,depName]
        if(tree[parentDep] == null){
          tree[parentDep] = []
        }
        tree[parentDep] += depID
        result[depID] = item
        //println result[parentDep]
      }
      tree[""]?.sort().each{id->
        def strID = result[id][0]
        def name = "- " + result[id][1]
        //strID = strID.padLeft(20);
        name = name.padRight(30)
        println "${name}${id}"
        tree[id]?.sort().each{subID->
          strID = result[subID][0]
          name = "  - " + result[subID][1]
          //strID = strID.padLeft(20);
          name = name.padRight(30)
          println "${name}${strID}"
        }
      }
      //result.sort().each{ println it; }
    }
  }

    println "---------------- states ------------------"
  clientIDs.each{ clientID ->
    println "client id: ${clientID} name: ${f.getClientDVProxy().getName(clientID)}"
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ objectID ->
      println "\tobject id: ${objectID} name: ${f.getPublicationDVProxy().getName(objectID)}"
      def stateIDs = f.getStateDVProxy().getIDs(objectID)
      stateIDs.each{ stateID->
        def stateName = f.getStateDVProxy().getName(stateID,"de")
        println "\t\tstate id: ${stateID} name: ${stateName}"
      }
    }
  }
/*
  println "---------------- macros ------------------"
  clientIDs.each{ clientID ->
    println "client id: ${clientID} name: ${f.getClientDVProxy().getName(clientID)}"
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ objectID ->
      println "\tobject id: ${objectID} name: ${f.getPublicationDVProxy().getName(objectID)}"
      def macroIDs = f.getMacroDVProxy().getAllMacroIDs(objectID,true,true)
      macroIDs.each{ macroID->
        def macroName = f.getMacroDVProxy().getName(macroID)
        def macroGroup = f.getMacroDVProxy().getGroup(macroID)
        def isActive = f.getMacroDVProxy().isActive(macroID)
        if(isActive){
          println "\t\tmacro id: ${macroID} name: ${macroName} group: ${macroGroup}"
        }
      }
    }
  }
/*
    println "---------------- styles ------------------"
  clientIDs.each{ clientID ->
    println "client id: ${clientID} name: ${f.getClientDVProxy().getName(clientID)}"
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ objectID ->
      println "\tobject id: ${objectID} name: ${f.getPublicationDVProxy().getName(objectID)}"
      def styleIDs = f.getStyleDVProxy().getAllIDs(clientID,objectID)
      styleIDs.each{ styleID->
        try{
          def styleName = f.getStyleDVProxy().getName(styleID,"de")
          def longID = ID.fromHexForm(styleID);
          println "\t\tstyle id: ${styleID} name: ${styleName} longID: ${longID}"
        }catch(Exception ex){
          println ex.message
        }

      }
    }
  }
*/
  
  f.getTransactionDVProxy().commit()
} catch (Exception e) {
  e.printStackTrace()
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}

