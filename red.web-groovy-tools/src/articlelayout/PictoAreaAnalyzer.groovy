package articlelayout
import distantvoices3.client.BasicClient
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.layout.pictogram.controller.PictogramController
import eu.red_web.library.compression.CompressionUtil
import eu.red_web.server.client.ClientFactory


def server = "rwlayout.saint-paul.lu"
def port = 8100
def user = "redweb_developer"
def passwd = "WZXVKmGmUQ"
def enc = "UTF-8"

TWSerializer.initTypeAliases()

BasicClient cl = new BasicClient(server, port, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])


def validNames = ["Leiste","Dachzeile","Hauptüberschrift", "Unterzeile","Vorspann"]

try{
  f.getTransactionDVProxy().open()
  def clientIDs = f.getClientDVProxy().getClientIDs()
  clientIDs.each{ clientID ->
    println "client id: ${clientID} name: ${f.getClientDVProxy().getName(clientID)}"
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ objectID ->
      println "object id: ${objectID} name: ${f.getPublicationDVProxy().getName(objectID)}"
      def pictoIDs = f.getPictogramDVProxy().getIDs(objectID)
      pictoIDs.each{ pictoID->
        def name1 = f.getPictogramDVProxy().getName(pictoID,0)
        def name2 = f.getPictogramDVProxy().getName(pictoID,1)
        def name3 = f.getPictogramDVProxy().getName(pictoID,2)
        def name4 = f.getPictogramDVProxy().getName(pictoID,3)
        def name5 = f.getPictogramDVProxy().getName(pictoID,4)
        def active = f.getPictogramDVProxy().isActive(pictoID)
        def deleted = f.getPictogramDVProxy().isMarkedForDeletion(pictoID)
        if(active && !deleted){
          name1 = name1 != null ? name1 + "\\" : ""
          name2 = name2 != null ? name2 + "\\"  : ""
          name3 = name3 != null ? name3 + "\\"  : ""
          name4 = name4 != null ? name4 + "\\"  : ""
          name5 = name5 != null ? name5 + "\\"  : ""
          def name = "${name1}${name2}${name3}${name4}${name5}"
          def pictoBytes = f.getPictogramDVProxy().download(pictoID)
          def pictoXML = new String(CompressionUtil.inflate(pictoBytes),enc)
  //        if(pictoXML.contains("Grundtextbereich")){
  //          println "picto id: ${pictoID} name: ${f.getPictogramDVProxy().getName(pictoID,0)}_${f.getPictogramDVProxy().getName(pictoID,1)}_${f.getPictogramDVProxy().getName(pictoID,2)}"
  //        }
          //println pictoXML
          PictogramController newPc = this.getClass().getClassLoader().loadClass("eu.red_web.layout.pictogram.controller.PictogramController").newInstance()
          newPc.fromDocument(pictoXML, true)
          
          boolean hasExclusiv = false
          def areaNames = newPc.getPictoareaListController().getPictoareanames()
          for(int i=0; i < newPc.getPictoareaListController().getList().size(); i++){
            def area = newPc.getPictoareaListController().getList().get(i)
            def nameCtrl = area.getNameController()
            def nameVal = nameCtrl.getText()
            def exclCtrl = nameCtrl.getExclusiveController() 
            if(exclCtrl.booleanValue() && validNames.contains(nameVal)){
              hasExclusiv = true
              break
            }
          }
          
          int textbereichPos = areaNames.indexOf("Textbereich")
          def prefix = ""
          if(textbereichPos >= 0 && textbereichPos != areaNames.size()-1){
            prefix = "!!!!!!!!!! "
            println "${prefix} Picto: ${name} contains the following areas: ${areaNames}"
          }
//          if(hasExclusiv){
//            prefix += "########## "
//          }
//          if(hasExclusiv){
//            println "${prefix} Picto: ${name} contains the following areas: ${areaNames}"
//          }
        }
        //byte[] newPCAsBytes = newPc.toDocument()
        //f.getPictogramDVProxy().upload(CompressionUtil.deflate(newPCAsBytes),clientID,objectID)
//        println "--------------------------------------------------------------------------------------";
      }
    }
  }
  f.getTransactionDVProxy().commit()
} catch (Exception e) {
  e.printStackTrace()
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}


