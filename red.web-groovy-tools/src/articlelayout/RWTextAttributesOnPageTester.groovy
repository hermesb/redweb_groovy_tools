package articlelayout
import distantvoices3.client.BasicClient
import eu.red_web.registry.embeddedserverimpl.ESInternalRegistry
import eu.red_web.server.client.ClientFactory
import eu.red_web.server.embedded.EmbeddedServer
import eu.red_web.server.embedded.MasterControl
import eu.red_web.server.embedded.sync.ConfigurationDataSynchronisation
import eu.red_web.server.embedded.sync.SynchronisationException
import eu.red_web.server.services.wrapper.EmbeddedIOLayer


def server = "produktion4.red-web.org"
def port = 8100
def user = "hermesb"
def passwd = "hermesb"
def enc = "UTF-8"

def sessionID = null;
try {
//  System.setProperty(Properties.HBM2DDL_MODE_OVERRIDE, "none");
  def urlStr = "http://${server}:8080/server/dv?dvPort=${port}"
  URL url = new URL(urlStr);
  EmbeddedServer.startup(url);
  MasterControl.init();
  String[] xx = MasterControl.getInstance().connect(user,passwd,"null","null");
  sessionID = xx[1];
  EmbeddedIOLayer.startup(user);
  boolean synchronize = true
  if (synchronize) {
    try {
      ConfigurationDataSynchronisation cfg = new ConfigurationDataSynchronisation()
      cfg.setSyncItems(null)
      cfg.synchronize(MasterControl.getInstance().getForegroundFactory())
    } catch (SynchronisationException e) {
      e.printStackTrace()
    }
  }
  new ESInternalRegistry().startup()
} catch (Exception  e) {
  e.printStackTrace()
}

BasicClient cl = MasterControl.getInstance().getForegroundDVClient()
ClientFactory f = new ClientFactory(cl);


query = """
  <query>
      <select>page_partpageplacements_partpage.id</select>
      <where>
        <and>
          <field name=\"page.template\" comparator=\"equal\" value=\"true\"/>
          <field name=\"page_publication.name\" comparator=\"equal\" value=\"Tageszeitung\"/>
        </and>
      </where>
  </query>
  """

println "Suche nach der Musterseiten"

try {
  f.getTransactionDVProxy().open()
  def resultStr = f.getPageDVProxy().search(query, false)
  f.getTransactionDVProxy().commit()

  //println resultStr;
  def result = new XmlSlurper().parseText(resultStr)
  def partPageIDs = []
  result.row.page.partpage.field.each{partPageIDs += it.@value.text()} //.each{partPageIDs += it}
  println partPageIDs

  //ArticleService articleService = new ArticleService(sessionID);
  //println articleService.isTemplate(ID.fromHexForm(partPageIDs[0]));


} catch (Exception e) {
  println "Fehler w�hrend der Suche der Seiten: ${e.getMessage()}"
  try {
    f.getTransactionDVProxy().rollback()
  } catch (Exception e2) {
    println "W�hrend des Zu�rckspielens der Transaktion ist ein Fehler aufgetreten: ${e2.getMessage()}"
  }
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}


