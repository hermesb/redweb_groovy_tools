package articlelayout
import distantvoices3.client.BasicClient
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.layout.pictogram.controller.PictogramController
import eu.red_web.library.compression.CompressionUtil
import eu.red_web.server.client.ClientFactory


def server = "redweb4.local"
def port = 8104
def user = "hermesb"
def passwd = "hermesb"
def enc = "UTF-8"

TWSerializer.initTypeAliases()


BasicClient cl = new BasicClient(server, port, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])


try{
  f.getTransactionDVProxy().open()
  def clientIDs = f.getClientDVProxy().getClientIDs()
  clientIDs.each{ clientID ->
    println "client name: ${f.getClientDVProxy().getName(clientID).padRight(36)} id: ${clientID}"
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ objectID ->
      def objectName = f.getPublicationDVProxy().getName(objectID)

      if(objectName.equals("Hallo")){
        def pictoIDs = f.getPictogramDVProxy().getIDs(objectID)
        //Lauf 1: Umbenennen aller Piktogruppen die nicht mit " CSS" enden in " ALT" und auf nicht aktiv setzen
        pictoIDs.each{ pictoID->
          def pictoBytes = f.getPictogramDVProxy().download(pictoID)
          def pictoXML = new String(CompressionUtil.inflate(pictoBytes),enc)
          PictogramController newPc = this.getClass().getClassLoader().loadClass("eu.red_web.layout.pictogram.controller.PictogramController").newInstance()
          newPc.fromDocument(pictoXML, true)
          
          def names = newPc.getNames()
          def name1 = names.size() > 0 ? names[0] : null
          def name2 = names.size() > 1 ? names[1] : null
          def name3 = names.size() > 2 ? names[2] : null
          
          f.getPictogramDVProxy().setActive(pictoID,true)

          def suffix = " ALT"
//          if(name1 != null && name1.endsWith(suffix)){
//            def index = name1.lastIndexOf(suffix)
//            if(index >= 0){
//              name1 = name1.substring(0,index)
//
//              String[] oldNames = newPc.getCommondataController().getNamelistController().getNames()
//              String[] newNames = new String[oldNames.length]
//              newNames[0] = name1
//              for(int i=1; i < oldNames.length; i++){
//                newNames[i] = oldNames[i]
//              }
//              println "old names: ${oldNames}"
//              println "new names: ${newNames}"
//              newPc.getCommondataController().getNamelistController().setNames(newNames)
//              byte[] newPCAsBytes = newPc.toDocument()
//              try{
//                f.getPictogramDVProxy().upload(CompressionUtil.deflate(newPCAsBytes),clientID,objectID)
//              }catch(Exception e){
//                println "Fehler beim hochladen des Artikellayouts: ${name1}/${name2}/${name3}"
//              }
//            }
//          }

//          if(name1 != null && !name1.endsWith(" ALT")){
//            name1 += " CSS"
//            
//            String[] oldNames = newPc.getCommondataController().getNamelistController().getNames()
//            String[] newNames = new String[oldNames.length] 
//            newNames[0] = name1
//            for(int i=1; i < oldNames.length; i++){
//              newNames[i] = oldNames[i]
//            }  
//            println "old names: ${oldNames}"
//            println "new names: ${newNames}"
//            newPc.getCommondataController().getNamelistController().setNames(newNames)
//            byte[] newPCAsBytes = newPc.toDocument()
//            try{
//              f.getPictogramDVProxy().upload(CompressionUtil.deflate(newPCAsBytes),clientID,objectID)
//            }catch(Exception e){
//              println "Fehler beim hochladen des Artikellayouts: ${name1}/${name2}/${name3}"
//            }
//          }
        }
      }
    }
  }
  f.getTransactionDVProxy().commit()
} catch (Exception e) {
  e.printStackTrace()
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}

