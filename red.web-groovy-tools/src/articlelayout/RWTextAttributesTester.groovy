package articlelayout
import distantvoices3.client.BasicClient
import eu.red_web.app.core.Version
import eu.red_web.commons.serializer.model.TWSerializer
import eu.red_web.layout.pictogram.controller.PictogramController
import eu.red_web.library.compression.CompressionUtil
import eu.red_web.server.client.ClientFactory


def server = "produktion4.red-web.org"
def port = 8100
def user = "hermesb"
def passwd = "hermesb"
def enc = "UTF-8"

if(Version.APPLICATION_VERSION_STRING.startsWith("3.9")){
  TWSerializer.initTypeAliases()
}

BasicClient cl = new BasicClient(server, port, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])


try{
  f.getTransactionDVProxy().open()
  def clientIDs = f.getClientDVProxy().getClientIDs()
  clientIDs.each{ clientID ->
    println "client name: ${f.getClientDVProxy().getName(clientID).padRight(36)} id: ${clientID}"
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ objectID ->
      println " object name: ${f.getPublicationDVProxy().getName(objectID).padRight(35)} id: ${objectID}"

      println "  Artikellayout"
      def result = [];
      def pictoIDs = f.getPictogramDVProxy().getIDs(objectID)
      pictoIDs.each{ id->
        def name1 = f.getPictogramDVProxy().getName(id,0)
        def name2 = f.getPictogramDVProxy().getName(id,1)
        def name3 = f.getPictogramDVProxy().getName(id,2)
        name1 = name1 != null ? name1 : ""
        name2 = name2 != null ? name2 : ""
        name3 = name3 != null ? name3 : ""
        def name = "${name1}\\${name2}\\${name3}"
        def item = "   ${name.padRight(45)}\tid: ${id}"

        def pictoBytes = f.getPictogramDVProxy().download(id)
        def pictoXML = new String(CompressionUtil.inflate(pictoBytes),enc);
//        if(pictoXML.contains("Grundtextbereich")){
//          println "picto id: ${pictoID} name: ${f.getPictogramDVProxy().getName(pictoID,0)}_${f.getPictogramDVProxy().getName(pictoID,1)}_${f.getPictogramDVProxy().getName(pictoID,2)}"
//        }
        PictogramController newPc = this.getClass().getClassLoader().loadClass("eu.red_web.layout.pictogram.controller.PictogramController").newInstance();
        newPc.fromDocument(pictoXML, true);
        newPc.getPictoareaListController().getList().each{pac->;
          if(pac == null){
            println "Pictogramm ${item} has PictoAreaController with value null"
          }
          else if(pac.getTextAttributes() == null){
            println "Pictogramm id: ${item} category: ${pac.getName()} has no RWTextAttributes"
          }
        }
      }
    }
  }
  f.getTransactionDVProxy().commit();
} catch (Exception e) {
  e.printStackTrace();
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}

