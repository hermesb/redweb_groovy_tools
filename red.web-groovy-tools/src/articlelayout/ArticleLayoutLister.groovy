package articlelayout
import distantvoices3.client.BasicClient
import eu.red_web.server.client.ClientFactory


def server = "rw4-users-1.red-web.org"
def port = 8100
def user = "hermesb"
def enc = "UTF-8"

BasicClient cl = new BasicClient(server, port, user, null)
ClientFactory f = new ClientFactory(cl)
String[] keys = f.getSessionDVProxy().login(user, null, passwd, null)
if (keys[1]==null)
  throw new Exception(keys[3])

cl.setPassword(keys[1])


try{
  f.getTransactionDVProxy().open()
  def clientIDs = f.getClientDVProxy().getClientIDs()
  clientIDs.each{ clientID ->
    println "client name: ${f.getClientDVProxy().getName(clientID).padRight(36)} id: ${clientID}"
    def objectIDs = f.getPublicationDVProxy().getObjectIDs(clientID)
    objectIDs.each{ objectID ->
      println " object name: ${f.getPublicationDVProxy().getName(objectID).padRight(35)} id: ${objectID}"

      def result = [:]
      def counter = [:]
      def pictoIDs = f.getPictogramDVProxy().getIDs(objectID)
      pictoIDs.each{ id->
        def name1 = f.getPictogramDVProxy().getName(id,0)
        def name2 = f.getPictogramDVProxy().getName(id,1)
        def name3 = f.getPictogramDVProxy().getName(id,2)
        name1 = name1 != null ? name1 : ""
        name2 = name2 != null ? name2 : ""
        name3 = name3 != null ? name3 : ""
        def name = "${name1}\\${name2}\\${name3}"
        def item = "   ${name.padRight(45)}\tid: ${id}"
        result[id]= name
        if(counter[name] == null){
          counter[name] = [id]
        }else{
          counter[name] += [id]
        }
      }
      counter.each {name->
        def count = counter[name]
        if(count != null && count.size() > 1){
          println "${name}:"
          count.each{id->
            println "\t${id}"
          }
        }
      }
    }
  }
  f.getTransactionDVProxy().commit()
} catch (Exception e) {
  e.printStackTrace()
}

try{
  f.getSessionDVProxy().logout(user,keys[1])
}catch(Exception e){
  println "exception during logout: ${e.getMessage()}"
}

