package font
import java.awt.Font;

import java.awt.Color
import java.awt.Font
import java.awt.Graphics2D
import java.awt.RenderingHints
import java.awt.geom.AffineTransform
import java.awt.geom.Rectangle2D
import java.awt.image.BufferedImage

import javax.imageio.ImageIO

String testStr = /ABD KLMN QSTVWXYZ/;
int imgHeight = 400;
BufferedImage buffImg = new BufferedImage(1500,imgHeight,BufferedImage.TYPE_INT_ARGB);
BufferedImage shadowImg = new BufferedImage(1500,imgHeight,BufferedImage.TYPE_INT_ARGB);
Graphics2D g2d = getGraphics(buffImg)
Graphics2D shadowGraphics = getGraphics(shadowImg)

try{
  File f = new File(".")
  print f.getAbsolutePath()
  Font tempFont = Font.createFont(Font.TYPE1_FONT, new File("/Users/hermesb/temp/HVN____.pfb"));
  Font tempFont2 = Font.createFont(Font.TYPE1_FONT, new File("/Users/hermesb/temp/LT_52134.PFB"));
  
  Font font = tempFont.deriveFont(50f);
  font = font.deriveFont(AffineTransform.getScaleInstance(1.0f, 1f));

    Font font2 = tempFont2.deriveFont(50f);
  font2 = font2.deriveFont(AffineTransform.getScaleInstance(1.0f, 1f));

  shadowGraphics.setFont(font);
  shadowGraphics.setColor(Color.red);
  shadowGraphics.drawString(testStr, 20, 250);
  //shadowGraphics.fillOval(50, 50, 1000, 350)

  float radius = 30f;
  float xOff = 150f;
  float yOff = 20f;
  float opacity = 1.0f;

  //  DropShadowPanel dsp = new DropShadowPanel(shadowImg);
  //  dsp.setShadowSize(10);
  //  dsp.setDistance(10);
  //  dsp.setShadowOpacity(1.0f);
  //  dsp.setShadowColor(Color.green);
  //  dsp.setAngle(0f);
  //  BufferedImage filteredImage = null;
  filteredImage = shadowImg;// dsp.createDropShadow(shadowImg);

  //  def shadow = new ShadowFilter();
  //  shadow.setAngle(0.0f);
  //  shadow.setDistance(2.0f);
  //  shadow.setRadius(5.0f);
  //  shadow.setOpacity(0.9f);
  //  def shadow = new GaussianFilter();
  //  shadow.setRadius(12f)
  //  shadowImg = shadow.filter(shadowImg,null);


  //g2d.drawImage(filteredImage,2,2,null);

  g2d.setFont(font);
  g2d.setColor(Color.black);
  g2d.drawString(testStr, 20, 100);
  g2d.setFont(font2);
  g2d.setColor(Color.black);
  g2d.drawString(testStr, 20, 250);


  g2d.dispose();
}catch(Exception e){
  e.printStackTrace();
}
try {
  File outputfile = new File("saved.png");
  ImageIO.write(buffImg, "png", outputfile);
} catch (IOException e){
}

Graphics2D getGraphics(BufferedImage buffImg){
  Graphics2D g2d = buffImg.createGraphics();
  g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
  g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
  g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
  g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,RenderingHints.VALUE_COLOR_RENDER_QUALITY);
  Rectangle2D rect = new Rectangle2D.Float(0f,0f,1500f,400f);
  //  g2d.setColor(Color.WHITE)
  //  g2d.fill(rect)
  return g2d;
}
